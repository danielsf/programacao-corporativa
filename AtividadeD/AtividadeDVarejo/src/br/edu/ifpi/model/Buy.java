package br.edu.ifpi.model;

import java.util.List;

import br.edu.ifpi.controller.CalculatorDiscount;

public class Buy {
	
	private double totalPrice;
	private List<ItemProduct> items;
	private Client client;
	
	//Init Buy:
	public void initBuy(){
		//the sum is here...
		double sum = 0;

		for(ItemProduct item : items){
			sum += item.getQuantity() * item.getPrice();
		}
		
		totalPrice = sum;
	}
	
	//Create Buy:
	public double commitBuy(){
		//Only One match:
		CalculatorDiscount cd = new CalculatorDiscount();
		totalPrice = cd.calculateNormal(this);
		return totalPrice;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public List<ItemProduct> getItems() {
		return items;
	}
	
	public void setItems(List<ItemProduct> items) {
		this.items = items;
	}
	
	public void addItem(ItemProduct item){
		if(!this.getItems().contains(item))
			this.getItems().add(item);
		else

			for (ItemProduct item : items){
				if ( item.getProduct().getName() == item.getProduct().getName() ){
					int newQuantity = this.item.getQuantity() + item.getQuantity();
					this.item.setQuantity(newQuantity);
				}
			}
	
	}
	
	public boolean removeItem(String productCode){
		for (int i = 0; i < this.getItems().size(); i++) {
			if(this.getItems().get(i).getProduct().getProductCode().equals(productCode)){
				this.getItems().remove(i);
				return true;
			}
		}
		
		return false;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}
}
