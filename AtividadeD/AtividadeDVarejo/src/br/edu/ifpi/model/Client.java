package br.edu.ifpi.model;

public class Client {
	private long clientCode;
	private String name;
	private String nationalId;
	private int points;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name.toUpperCase();
	}
	
	public String getNationalId() {
		return nationalId;
	}
	
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	
	public int getPoints() {
		return points;
	}
	
	public void setPoints(int punctuation) {
		this.points = punctuation;
	}

	public long getClientCode() {
		return clientCode;
	}

	public void setClientCode(long clientCode) {
		this.clientCode = clientCode;
	}
	
	

}
