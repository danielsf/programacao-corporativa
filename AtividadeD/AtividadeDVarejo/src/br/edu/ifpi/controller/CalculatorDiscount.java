package br.edu.ifpi.controller;

import br.edu.ifpi.helper.Discountable;
import br.edu.ifpi.helper.DiscountByBuyValue;
import br.edu.ifpi.helper.DiscountByItems;
import br.edu.ifpi.helper.DiscountByPoints;
import br.edu.ifpi.model.Buy;

public class CalculatorDiscount {

	public double calculateNormal(Buy buy){
		Discountable discountByValue = new DiscountByBuyValue();
		Discountable discountByItems = new DiscountByItems();
		Discountable discountByPoints = new DiscountByPoints();
		
		//Change TotalPrice, by reference:
		discountByItems.getPrice(buy);
		return discountByPoints.getPrice(buy) + discountByValue.getPrice(buy);
	}
	

}
