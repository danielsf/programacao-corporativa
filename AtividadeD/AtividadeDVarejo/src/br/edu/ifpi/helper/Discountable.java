package br.edu.ifpi.helper;

import br.edu.ifpi.model.Buy;

public interface Discountable {
	
	public double getPrice(Buy buy);
	
}
