package br.edu.ifpi.helper;

import java.util.List;

import br.edu.ifpi.model.Buy;
import br.edu.ifpi.model.ItemProduct;

public class DiscountByItems implements Discountable{
	
	public final double FINAL_PERCENTAGEM = (1 - 0.1); 
	
	@Override
	public double getPrice(Buy buy) {
		double sum = 0;
		List<ItemProduct> temporaryItems = buy.getItems();
		
		for (int i = 0; i < temporaryItems.size(); i++) {
			if(temporaryItems.get(i).getQuantity() > 20){
				sum += temporaryItems.get(i).getQuantity() * temporaryItems.get(i).getPrice() * FINAL_PERCENTAGEM;
			}
		}
		
		return sum;
	}
}
