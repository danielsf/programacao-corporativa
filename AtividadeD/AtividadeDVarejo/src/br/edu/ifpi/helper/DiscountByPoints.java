package br.edu.ifpi.helper;

import br.edu.ifpi.model.Buy;

public class DiscountByPoints implements Discountable{
	
	@Override
	public double getPrice(Buy buy) {
		return buy.getClient().getPoints() > 1000 ? buy.getTotalPrice() * 0.05 : 0;
	}
}
