package br.edu.ifpi.helper;

import br.edu.ifpi.model.Buy;

public class DiscountByBuyValue implements Discountable{

	@Override
	public double getPrice(Buy buy) {
		return (buy.getTotalPrice() > 1000) ? buy.getTotalPrice() * 0.1 : 0;
	}

}
