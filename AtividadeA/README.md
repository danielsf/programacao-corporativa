# ProgramacaoCorporativa

These are the files used on the activity A.

#### 1. Sistema de Transporte Urbano de Teresina

![Diagrama](SistemaTransporteUrbanoTeresina/ClassDiagram.png)

##### Sistema de Transporte Urbano de Teresina

Onibus – Passageiro N:N
Um onibus esta relacionado a diversos passageiros.
Um passageiro pode se relacionar com diversos onibus

Onibus – Funcionario N:N
Um onibus possui um numero especifico de funcionarios
Um funcionario pode estar relacionado a diversos onibus

Passageiro – Tipo de Passageiro N:1
Um passageiro pode pertencer a um tipo
Um tipo de passageiro possui muitos pertencentes

Onibus – Rota N:1
Um onibus tem uma rota
Um rota pode ser pertencida por varios onibus

Rota – Ponto Geografico N:N
Uma rota tem varios pontos
Um ponto geografico pode ser parte de varias rotas

Passageiro – Tipo de Passageiro N:1
Um passageiro pode pertencer a um tipo
Um tipo de passageiro possui muitos pertencentes

[Codigo](SistemaTransporteUrbanoTeresina/SistemaTransporteUrbanoTHE/)

#### 2. Sistema de Controle de Autorizacao/Autenticacao/Permissoes em um Sistema

![Diagrama](SistemaDeControleDeAutorizacaoAutenticacaoPermissoesEmUmSistema/ClassDiagram.png)

##### Sistema de Controle de Autorizacao/Autenticacao/Permissoes em um Sistema

Usuario – Perfil de Usuario N:1
Um usuario esta relacionado a um perfil.
Um perfil pode se relacionar com diversos usuarios

Perfil de Usuario – Permissao N:N
Um perfil possui um numero especifico de permissoes
Uma permissao pode estar relacionado a diversos perfis

Permissao – Tipo de Permissao N:1
Uma permissao pode pertencer a um tipo
Um tipo de permissao possui muitos permissoes

Recurso – Permissao 1:N
Um recurso tem varias permissoes relacionadas
Uma permissao pode ser relacionada a apenas um unico recurso

[Codigo](SistemaDeControleDeAutorizacaoAutenticacaoPermissoesEmUmSistema/SistemaAutorizacaoPermissao/)

#### 3. Sistema de Transporte Aereo

![Diagrama](SistemaDeTransporteAereo/ClassDiagram.png)

##### Sistema de Transporte Aereo

Passageiro – Bilhete 1:N
Um passageiro esta relacionado a varios bilhete.
Um bilhete pode se relacionar com um unico passageiro

Bilhete – Voo N:1
Um bilhete esta relacionado a um voo
Um voo pode estar relacionado a diversos bilhetes

Voo – Tipo de Voo N:1
Um voo pode pertencer a um tipo
Um tipo de voo possui muitos voos

Voo – Aeroporto N:1
Um voo esta relacionada a apenas um aeroporto
Um aeroporto pode possuir varios voos

Voo – Aviao N:1
Um voo pode pertencer a um aviao
Um aviao possui muitos voos

Cidade – Aeroporto 1:N
Uma cidade esta relacionada a varios aeroportos
Um aeroporto possui apenas um voo

[Codigo](SistemaDeTransporteAereo/SistemaTransporteAereo/)

#### 4. Programa de Fidelidade/Recompensa/Resgate

![Diagrama](ProgramaDeFidelidadeRecompensaResgate/ClassDiagram.png)

##### Sistema de Fidelidade/Recompensa/Resgate

Cliente – Recurso N:N
Um client esta relacionado a varios recursos.
Um recurso pode se relacionar varios clientes

Cliente – Recompensa N:1
Um cliente esta relacionado a varias recompensas
Uma recompensa pode estar relacionado a diversos clientes

[Codigo](ProgramaDeFidelidadeRecompensaResgate/SistemaFidelidadeRecompensa/)