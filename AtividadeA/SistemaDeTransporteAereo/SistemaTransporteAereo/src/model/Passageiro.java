/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Passageiro {
    private String nome;
    private String identificador;
    private Cidade origem;

    public Passageiro(String nome, String identificador, Cidade origem) {
        this.nome = nome;
        this.identificador = identificador;
        this.origem = origem;
    }

    public String getNome() {
        return nome;
    }

    public String getIdentificador() {
        return identificador;
    }

    public Cidade getOrigem() {
        return origem;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public void setOrigem(Cidade origem) {
        this.origem = origem;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.identificador);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Passageiro other = (Passageiro) obj;
        if (!Objects.equals(this.identificador, other.identificador)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Passageiro{" + "nome=" + nome + ", identificador=" + identificador + ", origem=" + origem + '}';
    }
    
    
}
