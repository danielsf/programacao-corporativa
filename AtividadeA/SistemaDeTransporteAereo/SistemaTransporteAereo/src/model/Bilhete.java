/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Bilhete {
    private Voo voo;
    private Passageiro passageiro;
    private String identificador;

    public Bilhete(Voo voo, Passageiro passageiro, String identificador) {
        this.voo = voo;
        this.passageiro = passageiro;
        this.identificador = identificador;
    }

    public Voo getVoo() {
        return voo;
    }

    public Passageiro getPassageiro() {
        return passageiro;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setVoo(Voo voo) {
        this.voo = voo;
    }

    public void setPassageiro(Passageiro passageiro) {
        this.passageiro = passageiro;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.voo);
        hash = 13 * hash + Objects.hashCode(this.passageiro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bilhete other = (Bilhete) obj;
        if (!Objects.equals(this.voo, other.voo)) {
            return false;
        }
        if (!Objects.equals(this.passageiro, other.passageiro)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bilhete{" + "voo=" + voo + ", passageiro=" + passageiro + ", identificador=" + identificador + '}';
    }
    
    
    
}
