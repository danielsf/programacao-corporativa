/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Voo {
    private String codigo;
    private String portao;
    private TipoVoo tipoVoo;
    private String dataHoraEmbarque;
    private String dataHoraSaida;
    private String dataHoraChegada;
    private Aeroporto origem;
    private Aeroporto destino;
    private Boolean hasEscala;
    private ArrayList<Aeroporto> listaEscalas;

    public Voo(String codigo, String portao, TipoVoo tipoVoo, String dataHoraEmbarque, String dataHoraSaida, String dataHoraChegada, Aeroporto origem, Aeroporto destino, Boolean hasEscala) {
        this.codigo = codigo;
        this.portao = portao;
        this.tipoVoo = tipoVoo;
        this.dataHoraEmbarque = dataHoraEmbarque;
        this.dataHoraSaida = dataHoraSaida;
        this.dataHoraChegada = dataHoraChegada;
        this.origem = origem;
        this.destino = destino;
        this.hasEscala = hasEscala;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getPortao() {
        return portao;
    }

    public TipoVoo getTipoVoo() {
        return tipoVoo;
    }

    public String getDataHoraEmbarque() {
        return dataHoraEmbarque;
    }

    public String getDataHoraSaida() {
        return dataHoraSaida;
    }

    public String getDataHoraChegada() {
        return dataHoraChegada;
    }

    public Aeroporto getOrigem() {
        return origem;
    }

    public Aeroporto getDestino() {
        return destino;
    }

    public Boolean getHasEscala() {
        return hasEscala;
    }

    public ArrayList<Aeroporto> getListaEscalas() {
        return listaEscalas;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setPortao(String portao) {
        this.portao = portao;
    }

    public void setTipoVoo(TipoVoo tipoVoo) {
        this.tipoVoo = tipoVoo;
    }

    public void setDataHoraEmbarque(String dataHoraEmbarque) {
        this.dataHoraEmbarque = dataHoraEmbarque;
    }

    public void setDataHoraSaida(String dataHoraSaida) {
        this.dataHoraSaida = dataHoraSaida;
    }

    public void setDataHoraChegada(String dataHoraChegada) {
        this.dataHoraChegada = dataHoraChegada;
    }

    public void setOrigem(Aeroporto origem) {
        this.origem = origem;
    }

    public void setDestino(Aeroporto destino) {
        this.destino = destino;
    }

    public void setHasEscala(Boolean hasEscala) {
        this.hasEscala = hasEscala;
    }

    public void setListaEscalas(ArrayList<Aeroporto> listaEscalas) {
        this.listaEscalas = listaEscalas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Voo other = (Voo) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        if (!Objects.equals(this.portao, other.portao)) {
            return false;
        }
        if (!Objects.equals(this.origem, other.origem)) {
            return false;
        }
        if (!Objects.equals(this.destino, other.destino)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Voo{" + "codigo=" + codigo + ", portao=" + portao + ", tipoVoo=" + tipoVoo + ", dataHoraEmbarque=" + dataHoraEmbarque + ", dataHoraSaida=" + dataHoraSaida + ", dataHoraChegada=" + dataHoraChegada + ", origem=" + origem + ", destino=" + destino + ", hasEscala=" + hasEscala + ", listaEscalas=" + listaEscalas + '}';
    }
    
    
    
}
