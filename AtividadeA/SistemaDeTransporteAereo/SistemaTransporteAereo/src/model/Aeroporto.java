/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Aeroporto {
    private String descricao;
    private String sigla;
    private Cidade cidade;
    private ArrayList<Voo> listaVoos;

    public Aeroporto(String descricao, String sigla, Cidade cidade) {
        this.descricao = descricao;
        this.sigla = sigla;
        this.cidade = cidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public ArrayList<Voo> getListaVoos() {
        return listaVoos;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public void setListaVoos(ArrayList<Voo> listaVoos) {
        this.listaVoos = listaVoos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.descricao);
        hash = 53 * hash + Objects.hashCode(this.sigla);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aeroporto other = (Aeroporto) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.sigla, other.sigla)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Aeroporto{" + "descricao=" + descricao + ", sigla=" + sigla + ", cidade=" + cidade + ", listaVoos=" + listaVoos + '}';
    }    
}
