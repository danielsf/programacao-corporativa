/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Permissao {
    private String descricao;
    private Recurso recurso;
    private TipoPermissao tipoPermissao;

    public Permissao(String descricao, Recurso recurso, TipoPermissao tipoPermissao) {
        this.descricao = descricao;
        this.recurso = recurso;
        this.tipoPermissao = tipoPermissao;
    }

    public String getDescricao() {
        return descricao;
    }

    public Recurso getRecurso() {
        return recurso;
    }

    public TipoPermissao getTipoPermissao() {
        return tipoPermissao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setRecurso(Recurso recurso) {
        this.recurso = recurso;
    }

    public void setTipoPermissao(TipoPermissao tipoPermissao) {
        this.tipoPermissao = tipoPermissao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Permissao other = (Permissao) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Permissao{" + "descricao=" + descricao + ", recurso=" + recurso + ", tipoPermissao=" + tipoPermissao + '}';
    }
    
    
    
}
