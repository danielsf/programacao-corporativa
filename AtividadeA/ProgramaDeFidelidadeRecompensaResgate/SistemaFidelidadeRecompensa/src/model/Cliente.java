/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Cliente {
    private String nome;
    private String cpf;
    private int totalPontos;
    private ArrayList<Recompensa> listaRecompensas;
    private ArrayList<Recurso> listaRecursos;
    private Date dataIngresso;
    private Date dataExpiracao;

    public Cliente(String nome, String cpf, Date dataIngresso) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataIngresso = dataIngresso;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public int getTotalPontos() {
        return totalPontos;
    }

    public ArrayList<Recompensa> getListaRecompensas() {
        return listaRecompensas;
    }

    public ArrayList<Recurso> getListaRecursos() {
        return listaRecursos;
    }

    public Date getDataIngresso() {
        return dataIngresso;
    }

    public Date getDataExpiracao() {
        return dataExpiracao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setTotalPontos(int totalPontos) {
        this.totalPontos = totalPontos;
    }

    public void setListaRecompensas(ArrayList<Recompensa> listaRecompensas) {
        this.listaRecompensas = listaRecompensas;
    }

    public void setListaRecursos(ArrayList<Recurso> listaRecursos) {
        this.listaRecursos = listaRecursos;
    }

    public void setDataIngresso(Date dataIngresso) {
        this.dataIngresso = dataIngresso;
    }

    public void setDataExpiracao(Date dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.nome);
        hash = 71 * hash + Objects.hashCode(this.cpf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.cpf, other.cpf)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + "nome=" + nome + ", cpf=" + cpf + ", totalPontos=" + totalPontos + ", listaRecompensas=" + listaRecompensas + ", listaRecursos=" + listaRecursos + ", dataIngresso=" + dataIngresso + ", dataExpiracao=" + dataExpiracao + '}';
    }
    
    
    
}
