/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Recompensa {
    private String descricao;
    private int valorMinimoResgate;
    public static int quantidadeEstoque;
    private Date dataExpiracao;

    public Recompensa(String descricao, int valorMinimoResgate, Date dataExpiracao) {
        this.descricao = descricao;
        this.valorMinimoResgate = valorMinimoResgate;
        this.dataExpiracao = dataExpiracao;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getValorMinimoResgate() {
        return valorMinimoResgate;
    }

    public static int getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public Date getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setValorMinimoResgate(int valorMinimoResgate) {
        this.valorMinimoResgate = valorMinimoResgate;
    }

    public static void setQuantidadeEstoque(int quantidadeEstoque) {
        Recompensa.quantidadeEstoque = quantidadeEstoque;
    }

    public void setDataExpiracao(Date dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recompensa other = (Recompensa) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Recompensa{" + "descricao=" + descricao + ", valorMinimoResgate=" + valorMinimoResgate + ", dataExpiracao=" + dataExpiracao + '}';
    }
    
    
    
}
