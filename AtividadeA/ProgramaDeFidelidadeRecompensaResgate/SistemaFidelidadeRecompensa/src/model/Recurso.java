/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Recurso {
    private String descricao;
    private int pontosAgregados;
    private ArrayList<Recompensa> excecoes;
    public static int quantidadeRecursos;

    public Recurso(String descricao, int pontosAgregados) {
        this.descricao = descricao;
        this.pontosAgregados = pontosAgregados;
    }

    public String getDescricao() {
        return descricao;
    }

    public int getPontosAgregados() {
        return pontosAgregados;
    }

    public ArrayList<Recompensa> getExcecoes() {
        return excecoes;
    }

    public static int getQuantidadeRecursos() {
        return quantidadeRecursos;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPontosAgregados(int pontosAgregados) {
        this.pontosAgregados = pontosAgregados;
    }

    public void setExcecoes(ArrayList<Recompensa> excecoes) {
        this.excecoes = excecoes;
    }

    public static void setQuantidadeRecursos(int quantidadeRecursos) {
        Recurso.quantidadeRecursos = quantidadeRecursos;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Recurso other = (Recurso) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Recurso{" + "descricao=" + descricao + ", pontosAgregados=" + pontosAgregados + ", excecoes=" + excecoes + '}';
    }
    
    
}
