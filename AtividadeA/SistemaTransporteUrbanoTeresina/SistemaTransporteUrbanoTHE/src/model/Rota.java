/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Rota {
    private ArrayList<PontoGeografico> listaPontos;
    private ArrayList<Onibus> listaOnibus;

    public Rota(ArrayList<PontoGeografico> listaPontos) {
        this.listaPontos = listaPontos;
    }

    public ArrayList<PontoGeografico> getListaPontos() {
        return listaPontos;
    }

    public ArrayList<Onibus> getListaOnibus() {
        return listaOnibus;
    }

    public void setListaPontos(ArrayList<PontoGeografico> listaPontos) {
        this.listaPontos = listaPontos;
    }

    public void setListaOnibus(ArrayList<Onibus> listaOnibus) {
        this.listaOnibus = listaOnibus;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Objects.hashCode(this.listaPontos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rota other = (Rota) obj;
        if (!Objects.equals(this.listaPontos, other.listaPontos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Rota{" + "listaPontos=" + listaPontos + ", listaOnibus=" + listaOnibus + '}';
    }
}
