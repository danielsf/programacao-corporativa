/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Onibus {
    private String codigo;
    private String modelo;
    private Rota rota;
    private ArrayList<Funcionario> funcionarios;
    private Double amonteEspecie;
    private ArrayList<Passageiro> passageiros;

    public Onibus(){
        
    }
    
    public Onibus(String codigo, String modelo, Rota rota) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.rota = rota;
    }

    public Onibus(String codigo, String modelo, Rota rota, ArrayList<Funcionario> funcionarios, Double amonteEspecie, ArrayList<Passageiro> passageiros) {
        this.codigo = codigo;
        this.modelo = modelo;
        this.rota = rota;
        this.funcionarios = funcionarios;
        this.amonteEspecie = amonteEspecie;
        this.passageiros = passageiros;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public Rota getRota() {
        return rota;
    }

    public ArrayList<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public Double getAmonteEspecie() {
        return amonteEspecie;
    }

    public ArrayList<Passageiro> getPassageiros() {
        return passageiros;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setRota(Rota rota) {
        this.rota = rota;
    }

    public void setFuncionarios(ArrayList<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    public void setAmonteEspecie(Double amonteEspecie) {
        this.amonteEspecie = amonteEspecie;
    }

    public void setPassageiros(ArrayList<Passageiro> passageiros) {
        this.passageiros = passageiros;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Onibus other = (Onibus) obj;
        if (!Objects.equals(this.rota, other.rota)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Onibus{" + "codigo=" + codigo + ", modelo=" + modelo + ", rota=" + rota + ", funcionarios=" + funcionarios + ", amonteEspecie=" + amonteEspecie + ", passageiros=" + passageiros + '}';
    }    
}
