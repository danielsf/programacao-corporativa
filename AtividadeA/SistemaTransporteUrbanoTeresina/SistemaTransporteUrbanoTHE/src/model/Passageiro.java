/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author daniel
 */
public class Passageiro {
    private String codigo;
    private TipoPassageiro tipoPassageiro;
    private String nome;
    private String cpf;
    
    public Passageiro(){
        
    }

    public Passageiro(String codigo, TipoPassageiro tipoPassageiro, String nome, String cpf) {
        this.codigo = codigo;
        this.tipoPassageiro = tipoPassageiro;
        this.nome = nome;
        this.cpf = cpf;
    }

    public String getCodigo() {
        return codigo;
    }

    public TipoPassageiro getTipoPassageiro() {
        return tipoPassageiro;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setTipoPassageiro(TipoPassageiro tipoPassageiro) {
        this.tipoPassageiro = tipoPassageiro;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Passageiro other = (Passageiro) obj;
        if (!Objects.equals(this.cpf, other.cpf)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Passageiro{" + "codigo=" + codigo + ", tipoPassageiro=" + tipoPassageiro + ", nome=" + nome + ", cpf=" + cpf + '}';
    }
    
    
    
}
