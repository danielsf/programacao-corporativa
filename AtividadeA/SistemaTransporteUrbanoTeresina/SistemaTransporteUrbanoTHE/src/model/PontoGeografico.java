/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author daniel
 */
public class PontoGeografico {
    private Double latitude;
    private Double longitude;
    private String rotulo;
    private Boolean hasIntegracao;
    private ArrayList<Rota> listaIntegracao;

    public PontoGeografico(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getRotulo() {
        return rotulo;
    }

    public Boolean getHasIntegracao() {
        return hasIntegracao;
    }

    public ArrayList<Rota> getListaIntegracao() {
        return listaIntegracao;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setRotulo(String rotulo) {
        this.rotulo = rotulo;
    }

    public void setHasIntegracao(Boolean hasIntegracao) {
        this.hasIntegracao = hasIntegracao;
    }

    public void setListaIntegracao(ArrayList<Rota> listaIntegracao) {
        this.listaIntegracao = listaIntegracao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.latitude);
        hash = 37 * hash + Objects.hashCode(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PontoGeografico other = (PontoGeografico) obj;
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PontoGeografico{" + "latitude=" + latitude + ", longitude=" + longitude + ", rotulo=" + rotulo + ", hasIntegracao=" + hasIntegracao + ", listaIntegracao=" + listaIntegracao + '}';
    }
    
    
    
    
    
}
