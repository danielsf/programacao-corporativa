from config import *

@app.route('/registration', methods=['GET'])
@login_required
def index_registration():
    events = query_db('SELECT * FROM tag')
    return events

@app.route('/registration/<int:id>')
@login_required
def view_registration(id):
    event = query_db('SELECT * FROM tag WHERE id = ?', [id], one=True)
    return event

@app.route('/registration/add', methods=['GET', 'POST'])
@login_required
def add_registration():
    name = request.form['name']
    feedback = query_db('INSERT INTO tag(name) VALUES (?)', [name])
    return feedback


