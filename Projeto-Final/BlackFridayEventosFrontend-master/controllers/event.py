from config import *

@app.route('/events')
def index_event():
    r = requests.get(URL+'events')
    return render_template('event/index.html', events=r.json())

@app.route('/events/view/<int:id>')
def view_event(id):
    r = requests.get(URL+'events/'+id)
    return render_template('event/view.html', event=r.json())

@app.route('/events/add', methods=['GET', 'POST'])
def add_event():
    if request.method == 'POST':
        r = requests.post(URL+'events', data=json.dumps(request.form), headers=HEADERS)
        if r.status_code == 201:
            flash(u'Event created successfully', 'success')
            return redirect(url_for('index_event'))
        else:
            flash(u'Not possible to create an event!', 'error')
        return render_template('event/add.html')
    else:
        return render_template('event/add.html')