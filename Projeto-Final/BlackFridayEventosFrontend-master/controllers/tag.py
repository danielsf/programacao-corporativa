from config import *

@app.route('/tag', methods=['GET'])
def index_tag():
    events = query_db('SELECT * FROM tag')
    return events
    #r = requests.get('https://blackfridayeventosapi.herokuapp.com/?name=mateus')
    #return render_template('index.html', message="All events", req=r,events=["A volta dos que nao foram", "Levante do Bolsoniro Silva Cardoso", "Encontro das Retas Paralelas"])

@app.route('/tag/<int:id>')
def view_tag(id):
    event = query_db('SELECT * FROM tag WHERE id = ?', [id], one=True)
    return event

@app.route('/tag', methods=['POST'])
def add_tag():
    name = request.form['name']
    feedback = query_db('INSERT INTO tag(name) VALUES (?)', [name])
    return feedback


