package com.eventos.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.eventos.model.activity.Local;
import com.eventos.model.activity.event.Address;

public class LocalTest {

	private Local local;
	@Before
	public void setUp() {
		local = new Local();
		this.local.setName("PIAUI");
		this.local.setAddress(new Address("Rua Olavo bilac", 1.1, 1.2));
	}
	@Test
	public void locals_should_be_empty_when_created() {
		assertEquals(local.getLocals(), new ArrayList<>());
	}
	@Test
	public void Local_should_not_be_accepted() {
		assertFalse(local.equals(null));
	}

	@Test(expected = RuntimeException.class)
	public void location_name_should_not_be_empty() {
		local.setName(null);
	}
	
	@Test(expected = RuntimeException.class)
	public void address_should_not_be_empty() {
		local.setAddress(null);
	}

}
