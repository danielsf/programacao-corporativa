package com.eventos.model;

import org.junit.Test;

import com.eventos.model.activity.Tag;

import static org.junit.Assert.assertTrue;

import org.junit.Before;

public class TagTest {
	private Tag tag;
	
	@Before
	public void setUp() {
		tag = new Tag("biscoito");
	}
	
	@Test
	public void tag_should_be_named() {
		assertTrue(tag.equals(new Tag("biscoito")));
	}
	
}
