package com.eventos.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import com.eventos.model.activity.Activity;
import com.eventos.model.activity.RegistrableActivity;
import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.event.Event;
import com.eventos.model.activity.payment.Coupon;
import com.eventos.model.activity.registration.Registration;

import org.junit.Before;

public class RegistrationTest {
	private Registration register;
	private User user;
	private Event event1;
	private Event event2;
	private RegistrableActivity activity1;
	private RegistrableActivity activity2;
	private RegistrableActivity activity3;
	private double tomadaTestActivityValue;
	private double guerraTestActivityValue;
	
	double atividade1Value = 12D;
	double atividade2Value = 12D;
	double atividade3Value = 12D;
	
	
	@Before
	public void setUp() {
		user = new User("ddanielsilva661@gmail.com", "daniel");
		event1 = new Event("Guerra de Bolachas", user);
		event2 = new Event("Volta dos que nao foram", user);

		tomadaTestActivityValue = 12.0d;
		guerraTestActivityValue = 13.0d;
		
		activity1 = new RegistrableActivity("Tomada da bastilha", event1);
		activity2 = new RegistrableActivity("Guerra do Urso", event2);
		activity3 = new RegistrableActivity("atividade 3",event1);
				
		register = new Registration(event1);
	}
	
	@Test
	public void registration_insert_activity() {		
		register.addActivity(activity1);
		assertEquals(register.getActivityItems().size(), 1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void registration_should_not_insert_activities_without_relationship() {		
		register.addActivity(activity1);
		register.addActivity(activity2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void registration_should_not_insert_activities_from_paid_registration() {		
		event1.addSecondary(event2);
		register = new Registration(event1);
		register.addActivity(activity1);
		register.pay(user);
		register.addActivity(activity2);
	}
	
	@Test
	public void registration_should_insert_activities_from_secondary() {		
		event1.addSecondary(event2);
		register = new Registration(event1);
		register.addActivity(activity1);
		register.addActivity(activity2);
		assertEquals(register.getActivityItems().size(), 2);
	}
	
	@Test
	public void registration_should_give_the_right_sum() {				
		event1.addSecondary(event2);
		activity1.setPrice(tomadaTestActivityValue);
		activity2.setPrice(guerraTestActivityValue);
		register = new Registration(event1);
		register.addActivity(activity1);
		register.addActivity(activity2);
		assertEquals(register.getTotalValue().doubleValue(), (guerraTestActivityValue + tomadaTestActivityValue), 0.0001);
	}
	
	@Test
	public void registration_should_give_the_right_sum_with_coupon() {			
		
		event1.addSecondary(event2);
		
		activity1.setPrice(atividade1Value);
		activity3.setPrice(atividade3Value);
		
		event1.addActivity(activity1);
		event1.addActivity(activity3);
		
		register = new Registration(event1);
		
		register.addActivity(activity1);
		register.addActivity(activity3);
		
		Coupon coupon = Coupon.getNormalCoupon(30.0);
		register.addCoupon(coupon);
		
		double valorCalculadoDoDesconto = ((atividade1Value + atividade3Value) - (atividade1Value + atividade3Value) * 0.3);
		System.out.println("valor calculado do desconto: "+valorCalculadoDoDesconto);
		System.out.println("valor do desconto no cupom: "+coupon.getDiscountPercent());
		
		assertEquals(register.getTotalValue(), valorCalculadoDoDesconto , 0.1);
	}
}