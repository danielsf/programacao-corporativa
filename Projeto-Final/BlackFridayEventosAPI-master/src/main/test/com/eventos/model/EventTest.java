package com.eventos.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.event.Event;

import org.junit.Before;

public class EventTest {
	private Event event;
	private User user;
	
	@Before
	public void setUp() {
		user = new User("daniel", "daniel");
		event = new Event("daniel@gmail.com", user);
	}
	
	@Test(expected = RuntimeException.class)
	public void user_should_not_be_filled_with_empty_creator() {
		event.setCreator(null);
	}
	
	@Test(expected = RuntimeException.class)
	public void user_should_not_be_filled_with_empty_name() {
		event.setName(null);
	}
	
	@Test
	public void activities_and_creator_should_be_valid() {
		assertEquals(!event.getCreator().equals(null) 
				&& event.getActivities().equals(new ArrayList<>()), true);
	}
	
	@Test
	public void relationships_should_be_valid_and_empty() {
		assertEquals(event.getPartners().equals(new ArrayList<>()), true);
	}
}