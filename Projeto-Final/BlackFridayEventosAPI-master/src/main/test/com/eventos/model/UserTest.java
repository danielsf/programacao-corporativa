package com.eventos.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.eventos.model.activity.authentication.User;

public class UserTest {
	private User user;
	
	@Before
	public void setUp() {
		user = new User("daniel@gmail.com", "daniel");
	}
	
	@Test(expected = RuntimeException.class)
	public void user_should_not_be_filled_with_empty_email() {
		user.setEmail(null);
	}
	
	@Test(expected = RuntimeException.class)
	public void user_should_not_be_filled_with_empty_password() {
		user.setPassword(null);
	}
	
	@Test
	public void events_and_registrations_should_be_valid_and_empty() {
		assertEquals(user.getRegistrations().equals(new ArrayList<>()) 
				&& user.getEvents().equals(new ArrayList<>()), true);
	}
}