package com.eventos.repository;

import com.eventos.model.activity.registration.Registration;

public interface RegistrationRepository extends ModelRepository<Registration, Long>{
	
}
