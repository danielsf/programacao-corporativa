package com.eventos.repository;

import com.eventos.model.activity.RegistrableActivity;

public interface EventActivityRepository extends ModelRepository<RegistrableActivity, Long>{
	
}
