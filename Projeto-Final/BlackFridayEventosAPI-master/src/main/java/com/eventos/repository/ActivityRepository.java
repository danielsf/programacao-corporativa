package com.eventos.repository;

import com.eventos.model.activity.Activity;

public interface ActivityRepository extends ModelRepository<Activity, Long>{
	
}
