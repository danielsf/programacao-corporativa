package com.eventos.repository;

import com.eventos.model.activity.payment.Payment;

public interface PaymentRepository extends ModelRepository<Payment, Long>{
	
}
