package com.eventos.repository;

import com.eventos.model.activity.Responsable;

public interface ResponsableRepository extends ModelRepository<Responsable, Long>{

}
