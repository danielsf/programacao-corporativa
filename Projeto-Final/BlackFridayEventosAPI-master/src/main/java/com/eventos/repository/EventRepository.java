package com.eventos.repository;

import com.eventos.model.activity.event.Event;

public interface EventRepository extends ModelRepository<Event, Long> {
	
}
