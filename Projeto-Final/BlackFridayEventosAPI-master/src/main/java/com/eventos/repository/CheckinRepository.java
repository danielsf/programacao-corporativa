package com.eventos.repository;

import com.eventos.model.activity.Checkin;

public interface CheckinRepository extends ModelRepository<Checkin, Long>{

}
