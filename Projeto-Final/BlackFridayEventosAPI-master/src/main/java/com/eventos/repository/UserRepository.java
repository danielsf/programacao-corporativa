package com.eventos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.eventos.model.activity.authentication.User;

public interface UserRepository extends ModelRepository<User, Long> {
	// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
	// CRUD refers Create, Read, Update, Delete
	@Query("SELECT u FROM User u WHERE LOWER(u.email) = LOWER(:email) AND LOWER(u.password) = LOWER(:password)")
    public List<User> findByEmailAndPassword(@Param("email") String email, @Param("password") String password);
}