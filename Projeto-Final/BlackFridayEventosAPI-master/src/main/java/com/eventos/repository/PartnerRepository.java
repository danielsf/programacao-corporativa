package com.eventos.repository;

import com.eventos.model.activity.event.Partner;

public interface PartnerRepository extends ModelRepository<Partner, Long>{
	
}
