package com.eventos.repository;

import com.eventos.model.activity.Section;

public interface SectionRepository extends ModelRepository<Section, Long>{
	
}
