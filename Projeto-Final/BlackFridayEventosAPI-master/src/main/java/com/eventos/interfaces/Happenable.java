package com.eventos.interfaces;

import com.eventos.model.activity.event.Event;

public interface Happenable {
	Event getEvent();
}
