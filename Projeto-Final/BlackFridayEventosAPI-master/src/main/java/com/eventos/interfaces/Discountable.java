package com.eventos.interfaces;

public interface Discountable {
	
	Double getValue();
	
}
