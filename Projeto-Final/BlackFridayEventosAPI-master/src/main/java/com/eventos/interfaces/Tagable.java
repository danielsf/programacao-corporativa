package com.eventos.interfaces;
import java.util.*;

import com.eventos.model.activity.*;

public interface Tagable {
	List<Tag> getTags();
}
