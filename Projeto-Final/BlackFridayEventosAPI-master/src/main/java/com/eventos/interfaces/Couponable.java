package com.eventos.interfaces;

public interface Couponable {
	Double getDiscountPercent();
}
