package com.eventos.interfaces;

public interface Authenticable{
	String getEmail();
	String getPassword();
}
