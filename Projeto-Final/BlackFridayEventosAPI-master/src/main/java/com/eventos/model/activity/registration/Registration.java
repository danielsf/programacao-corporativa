package com.eventos.model.activity.registration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.eventos.interfaces.Calculable;
import com.eventos.model.activity.RegistrableActivity;
import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.event.Event;
import com.eventos.model.activity.payment.Coupon;
import com.eventos.model.activity.payment.Payment;
import com.eventos.model.activity.payment.PaymentStatus;
import com.eventos.utils.Hash;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.*;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Registration implements Calculable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date registrationDate;
	@Column
	private Double value;
	@Column
	private Double totalValue;
	@Column(nullable=false)
	private RegistrationStatus registrationStatus;
	@Column
	private String registrationCode;
	
	@ManyToOne
    @JoinColumn(name="user_id", nullable=false)
	@JsonBackReference
	private User user;
	
	@Transient
	private Event event;
	@Transient
	private Payment payment;
	@Transient
	private List<RegistrableActivity> activityItems;
	@Transient
	private List<Coupon> coupons; 

	private Registration() {
		this.value = 0.0;
		this.payment = new Payment(this);
		this.registrationDate = new Date();
		this.totalValue = 0.0;
		this.registrationStatus = RegistrationStatus.STARTED;
		this.registrationCode = Hash.getStringHash(20);
		this.activityItems = new ArrayList<>();
		this.coupons = new ArrayList<>();
	}
	
	public Registration(Event event) {
		this();
		this.event = event;
	}
	
	public void pay(User creator) {
		this.payment.setCreator(creator);
		this.payment.setPaymentStatus(PaymentStatus.PAID);
	}
	
	public void addActivity(RegistrableActivity activity){
		//Check if is paid:
		
		if(this.payment.getPaymentStatus().equals(PaymentStatus.PAID)) {
			throw new IllegalArgumentException("This activity is paid");
		}

		for(Event secondary : this.event.getSecondaryEvents()) {
			if(secondary.equals(activity.getEvent())) {
				this.activityItems.add(activity);
				return;
			}
		}
		
		if (!this.getEvent().equals(activity.getEvent())) 
			throw new IllegalArgumentException("This activity cannot be combined to the registration");
		
		for (RegistrableActivity nonActivity : activity.getNonParalel()) {
			if(activity.equals(nonActivity)) {
				throw new IllegalArgumentException("This activity cannot be aggregated");
			}
		}
		this.activityItems.add(activity);
	}	
	
	public Double getTotalValue() {
		this.totalValue =  this.getValue();
		
		for (Coupon coupon : this.coupons) {
			this.totalValue -= this.totalValue * (coupon.getDiscountPercent() / 100.0d);
		}
		
		return totalValue;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public void setActivityItems(List<RegistrableActivity> activityItems) {
		this.activityItems = activityItems;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}

	@Override
	public Double getValue() {
		if (this.activityItems.size() == 0) return this.value;
		//Get All Items:
		for (RegistrableActivity item : this.activityItems){
			this.value += item.getPrice();
		}
		//Applying All coupons
		
		return this.value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public RegistrationStatus getRegistrationStatus() {
		return registrationStatus;
	}

	public void setRegistrationStatus(RegistrationStatus registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	public String getRegistrationCode() {
		return registrationCode;
	}

	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public List<RegistrableActivity> getActivityItems() {
		return activityItems;
	}

	public List<Coupon> getCoupons() {
		return coupons;
	}
	
	public void setTotalValue(Double value) {
		this.value = value;
	}

	public void addCoupon(Coupon coupon) {
		this.coupons.add(coupon);
	}
}
