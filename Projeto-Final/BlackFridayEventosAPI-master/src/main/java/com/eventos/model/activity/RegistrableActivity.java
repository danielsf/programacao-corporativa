package com.eventos.model.activity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.eventos.model.activity.event.Event;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class RegistrableActivity extends Activity{
	@Column
	private Double price;
	@Column
	private RegistrableActivityType activityType;
	@Transient
	private List<RegistrableActivity> nonParalel;
	@Transient
	private List<Tag> tags;
	@Transient
	private List<Local> local;
	
	public RegistrableActivity(String name) {
		super(name);
		this.nonParalel = new ArrayList<>();
	}
	
	public RegistrableActivity(String name, Event event) {
		//super(name, event);
		this(name);
		
		// @Daniels, when EventActivity is created, we shall call the parent Event method
		// .addActivity to add that activity, as below 
		//event.addActivity(this);
		//
		
		this.event = event;
		
		
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public boolean isAGrandLecture(){
		return this.activityType.equals(RegistrableActivityType.GRANDE_LECTURE);
	}
	
	public RegistrableActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(RegistrableActivityType activityType) {
		this.activityType = activityType;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public List<RegistrableActivity> getNonParalel() {
		return nonParalel;
	}

	public void setNonParalel(List<RegistrableActivity> nonParalel) {
		this.nonParalel = nonParalel;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Local> getLocal() {
		return local;
	}

	public void setLocal(List<Local> local) {
		this.local = local;
	}
}
