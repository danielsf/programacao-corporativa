package com.eventos.model.activity.event;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply=true)
public class AddressConverter implements AttributeConverter<Address, String>{
	private static final String SEPARATOR = "|";

	 @Override
	 public String convertToDatabaseColumn(Address address) {
		 if(address == null)
			 return null;
		 StringBuilder sb = new StringBuilder();
		 sb.append(address.getName().toString()).append(SEPARATOR)
	     .append(address.getLatitude().toString()).append(SEPARATOR)
	     .append(address.getLongitude().toString()).append(SEPARATOR);
		 return sb.toString();
	 }

	 @Override
	 public Address convertToEntityAttribute(String rangeString) {
		 if(rangeString == null)
			 return null;
		String[] range = rangeString.split(SEPARATOR);
		Address address =  new Address(range[0], Double.parseDouble(range[1]), Double.parseDouble(range[2]));
		return address;
	 }
}
