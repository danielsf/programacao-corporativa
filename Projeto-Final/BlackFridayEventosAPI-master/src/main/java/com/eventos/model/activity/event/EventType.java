package com.eventos.model.activity.event;

public enum EventType {
	MASTER_EVENT, SATELLITE_EVENT;
}
