package com.eventos.model.activity.payment;

public enum PaymentStatus {
	PAID, PENDING, UNPAID;
}
