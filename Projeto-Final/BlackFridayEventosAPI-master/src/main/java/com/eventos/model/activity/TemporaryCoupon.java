package com.eventos.model.activity;

import com.eventos.model.activity.payment.Coupon;

public class TemporaryCoupon extends Coupon {
	
	private static TemporaryCoupon temporaryCoupon = new TemporaryCoupon();
	
	private TemporaryCoupon() {
		super();
	}
	
	private TemporaryCoupon(Double discountPercent) {
		this();
		temporaryCoupon.discountPercent = discountPercent;
	}
	
	public static TemporaryCoupon getTemporaryCoupon(Double discountPercent) {
		return temporaryCoupon;
	}
}
