package com.eventos.model.activity.payment;

public enum CouponType{
	NORMAL_COUPON, AUTOMATIC_COUPON;
}
