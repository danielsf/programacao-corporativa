package com.eventos.model.activity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.eventos.model.activity.event.Event;
import com.eventos.utils.DateTimeRange;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public abstract class Activity{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	@Column(nullable=false)
	protected String name;
	@Column
	protected String description;
	@Transient
	protected DateTimeRange duration;
	@Transient
	protected Event event;
	@Transient
	protected ArrayList<Responsable> responsables;	
	
	private Activity(){
		this.responsables = new ArrayList<>();
	}
	
	public Activity(String name) {
		this();
		this.name = name;	
	}
	
	public Activity(String name, Event event) {
		this();
		this.event = event;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<Responsable> getResponsables() {
		return responsables;
	}

	public void setResponsables(ArrayList<Responsable> responsables) {
		this.responsables = responsables;
	}
}
