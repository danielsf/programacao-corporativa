package com.eventos.model.activity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.eventos.model.activity.authentication.User;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Section {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String name;
	@Transient
	private List<Tag> tags;
	@Transient
	private List<RegistrableActivity> registrableActivities;
	@Transient
	private List<User> coordinators;
	@Transient
	private List<String> coupons;
	
	private Section() {
		this.tags = new ArrayList<>();
		this.registrableActivities = new ArrayList<>();
		this.coordinators = new ArrayList<>();
		this.coupons = new ArrayList<>();
	}
	
	public Section(String name) {
		this();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<RegistrableActivity> getEventActivities() {
		return registrableActivities;
	}

	public void setEventActivities(List<RegistrableActivity> registrableActivities) {
		this.registrableActivities = registrableActivities;
	}

	public List<User> getCoordinators() {
		return coordinators;
	}

	public void setCoordinators(List<User> coordinators) {
		this.coordinators = coordinators;
	}

	public List<String> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<String> coupons) {
		this.coupons = coupons;
	}
}
