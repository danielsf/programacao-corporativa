package com.eventos.model.activity.event;

import java.util.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.eventos.interfaces.Tagable;
import com.eventos.model.activity.Activity;
import com.eventos.model.activity.Local;
import com.eventos.model.activity.Tag;
import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.payment.Coupon;
import com.eventos.utils.DateTimeRange;
import com.eventos.utils.GenericValidator;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Event implements Tagable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String name;
	@Column
	private String description;
	@Column(nullable=false)
	private String photo;
	@Column
	private Double price;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@JsonIgnore
	private Date createdDate;
	@Column
	private EventStatus eventStatus;
	@Column
	private EventType eventType;
	@ManyToOne
    @JoinColumn(name="user_id", nullable=true)
	@JsonBackReference
	private User creator;
	@Transient
	private List<User> administrators;
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name="event_id")
	private Event parentEvent;
	@OneToMany
	private List<Event> secondaryEvents;
	@Transient
	private DateTimeRange duration;
	@Column
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	@Column
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date endDate;
	@OneToMany
	@JoinColumn(name="event_id")
	private List<Tag> tags;
	@Column
	@Convert(converter=AddressConverter.class)
	private Address address;
	@OneToMany
	@JoinColumn(name="event_id")
	private List<Partner> partners;
	@OneToMany
	@JoinColumn(name="event_id")
	private List<Activity> activities;
	@OneToMany
	@JoinColumn(name="event_id")
	private List<Coupon> discountCoupons;
	@Transient
	private List<Local> locals;
	@ManyToOne(fetch=FetchType.LAZY)
	private Coupon temporaryCoupon;
	
	private Event() {
		this.activities = new ArrayList<>();
		this.tags = new ArrayList<>();
		this.partners = new ArrayList<>();
		this.secondaryEvents = new ArrayList<>();
		this.discountCoupons = new ArrayList<>();
		this.eventType = EventType.MASTER_EVENT;
		this.parentEvent = this;
		this.duration = new DateTimeRange();
		this.createdDate = Date.from(Instant.now());
		this.eventStatus = EventStatus.NEW;
		this.administrators = new ArrayList<>();
		this.duration = new DateTimeRange();
		this.startDate = this.duration.getStartDate();
		this.endDate = this.duration.getEndDate();
		this.locals = new ArrayList<>();
	}
	
	public void addActivity(String name) {
		//TODO
	}
	
	public List<Local> getLocals() {
		return locals;
	}

	public void setLocals(List<Local> locals) {
		this.locals = locals;
	}

	public Event getParentEvent() {
		return parentEvent;
	}

	public Event(String name, User creator) {
		this();
		this.setName(name);
		this.setCreator(creator);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		GenericValidator.checkEmptyString(name, "Name");
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public User getCreator() {
		return creator;
	}
	
	public void setCreator(User creator) {
		GenericValidator.checkNull(creator, "Creator");
		this.creator = creator;
	}
	
	public DateTimeRange getDuration() {
		return duration;
	}
	
	public void setDuration(DateTimeRange duration) {
		this.duration = duration;
	}
	
	public EventStatus getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}
	public EventType getEventType() {
		return eventType;
	}
	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}
	public void setParentEvent(Event event){
		this.parentEvent = event;
	}
	public Event getParentEvent(Event event){
		return this.parentEvent;
	}
	public List<User> getAdministrators() {
		return administrators;
	}
	
	private void setAdministrators(List<User> administrators) {
		this.administrators = administrators;
	}
	
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Partner> getPartners() {
		return partners;
	}
	public void setPartners(List<Partner> partners) {
		this.partners = partners;
	}
	public List<Event> getSecondaryEvents() {
		return secondaryEvents;
	}
	public void setSecondaryEvents(List<Event> secondaryEvents) {
		this.secondaryEvents = secondaryEvents;
	}
	public List<Activity> getActivities() {
		return activities;
	}
	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	public List<Coupon> getDiscountCoupons() {
		return discountCoupons;
	}
	public void setDiscountCoupons(List<Coupon> discountCoupons) {
		this.discountCoupons = discountCoupons;
	}
	
	public Coupon getTemporaryCoupon() {
		return temporaryCoupon;
	}

	public void setTemporaryCoupon(Coupon temporaryCoupon) {
		this.temporaryCoupon = temporaryCoupon;
	}

	public void addActivity(Activity activity) {
		if (activity.getEvent().equals(this)) {
			this.activities.add(activity);
		}
	}
	
	public void addSecondary(Event event) {
		if(!event.equals(this)) {
			if(!event.getEventType().equals(EventType.SATELLITE_EVENT)) {
				event.setEventType(EventType.SATELLITE_EVENT);
				this.secondaryEvents.add(event);
				event.setParentEvent(this);
			}
		}
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		this.duration.setStartDate(this.startDate);
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		this.duration.setEndDate(this.endDate);
	}
}