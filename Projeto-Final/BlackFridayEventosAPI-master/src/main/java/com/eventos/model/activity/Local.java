package com.eventos.model.activity;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import com.eventos.model.activity.event.Address;
import com.eventos.utils.GenericValidator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Local{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable=false)
	private String name;
	@Column(nullable=false)
	private Address address;
	@Transient
	private List<Local> locals;
	
	public Local(){	
		locals = new ArrayList<>();
	}
		
	public List<Local> getLocals() {
		return locals;
	}

	public void setLocals(List<Local> locals) {
		this.locals = locals;
	}

	
	public Integer getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public Address getAddress(){
		return this.address;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public void setName(String name){
		GenericValidator.checkNull(name, "name");
		this.name = name;
	}
	
	public void setAddress(Address address){
		GenericValidator.checkNull(address, "address");
		this.address = address;
	}
}
