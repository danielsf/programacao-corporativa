package com.eventos.model.activity.authentication;

public enum UserStatus {
	UNCONFIRMED, CONFIRMED;
}
