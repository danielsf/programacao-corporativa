package com.eventos.model.activity.event;

public class Address {
	private String name;
    private Double longitude;
    private Double latitude;
    
	public Address() {
		this.name = "Grenwich Meridian";
		this.latitude = 0d;
		this.longitude = 0d;
	}
	
	public Address(String name, Double latitude, Double longitude) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
}
