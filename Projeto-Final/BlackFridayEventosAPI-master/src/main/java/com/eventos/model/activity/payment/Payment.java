package com.eventos.model.activity.payment;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.registration.Registration;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Payment{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date paymentDate;
	@Column
	private PaymentStatus paymentStatus;
	
	@Transient
	private Registration registration;
	@Transient
	//Aplicar refatoração de nome, especificar quem ta fazendo
	//ação de pagamento
	//TODO
	private User creator;
	
	private Payment() {
		this.paymentDate = new Date();
		this.paymentStatus = PaymentStatus.UNPAID;
	}
	
	public Payment(Registration registration) {
		this();
		this.registration = registration;
	}
	
	public Payment(Registration registration, User creator) {
		this(registration);
		this.creator = creator;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Registration getRegistration() {
		return registration;
	}

	public void setRegistration(Registration registration) {
		this.registration = registration;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
}
