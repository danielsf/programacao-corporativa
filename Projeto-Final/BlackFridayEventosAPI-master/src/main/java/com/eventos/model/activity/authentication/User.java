package com.eventos.model.activity.authentication;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.eventos.model.activity.Tag;
import com.eventos.model.activity.event.Event;
import com.eventos.model.activity.registration.Registration;
import com.eventos.utils.GenericValidator;
import com.eventos.utils.Hash;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Table(name = "app_user")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Column(nullable=false)
    private String fullname;
    @Column(unique=true, nullable=false)
    private String email;
    @Column(nullable=false,length=128)
    private String password;
    @Column
    @JsonIgnore
    private String confirmationCode;
	@Column
	private UserStatus userStatus;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="creator")
	@JsonManagedReference
	private List<Event> createdEvents;
	@ManyToMany(cascade = { 
        CascadeType.PERSIST, 
        CascadeType.MERGE,
        CascadeType.ALL
    })
    @JoinTable(name = "user_tag",
        joinColumns =  @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
	private List<Tag> tags;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user")
	@JsonManagedReference
	private List<Registration> registrations;
    
	@ManyToMany(cascade = { 
        CascadeType.PERSIST, 
        CascadeType.MERGE,
        CascadeType.ALL
    })
	@Transient
    private List<Event> administratedEvents;
	
    private User() {
    	this.createdEvents = new ArrayList<>();
    	this.registrations = new ArrayList<>();
    	this.tags = new ArrayList<>();
    	this.administratedEvents = new ArrayList<>();
    	this.userStatus = UserStatus.UNCONFIRMED;
    	this.confirmationCode = Hash.getStringHash(20);
    }
    
    public User(String email, String password) {
    	this();
    	this.email = email;
    	this.setPassword(password);
	}
    
    
	public Long getId() {
		return id;
	}
	
    public void setId(Long id) {
		this.id = id;
	}
	
    public String getEmail() {
    	return email;
	}
	
    public void setEmail(String email) {
    	GenericValidator.checkEmptyString(email, "E-mail");
    	this.email = email;
	}
	
    public String getPassword() {
		return password;
	}
	
    public void setPassword(String password) {
    	GenericValidator.checkEmptyString(password, "Password");
    	this.password = Hash.getSHA1(password);
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}
	
    public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}
	
    public List<Tag> getTags() {
		return tags;
	}
	
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public List<Event> getEvents() {
		return createdEvents;
	}
	
	public void setEvents(List<Event> events) {
		this.createdEvents = events;
	}
	
	public List<Registration> getRegistrations() {
		return registrations;
	}
	
	public void setRegistrations(List<Registration> registrations) {
		this.registrations = registrations;
	}
	
	public List<Event> getAdministratedEvents() {
		return administratedEvents;
	}
	
	public void setAdministrations(List<Event> administratedEvents) {
		this.administratedEvents = administratedEvents;
	}

	public void setFullname(String fullname) {
		GenericValidator.checkEmptyString(fullname, "Fullname");
		this.fullname = fullname;
	}
	
	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public String getFullname() {
		return fullname;
	}
	
	public boolean isValidConfirmationCode(String confirmationCode) {
		return this.confirmationCode.equals(confirmationCode);
	}
	
	static User getEmptyUser() {
		return new User();
	}

	@Override
	public String toString() {
		return String.format(
				"{id=%s, fullname=%s, email=%s, password=%s, confirmationCode=%s, userStatus=%s, tags=%s, events=%s, registrations=%s, administratedEvents=%s}",
				id, fullname, email, password, confirmationCode, userStatus, tags, createdEvents, registrations,
				administratedEvents);
	}
}