package com.eventos.model.activity.payment;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.eventos.interfaces.Discountable;
import com.eventos.utils.DateTimeRange;
import com.eventos.utils.Hash;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, 
property = "id")
public class Coupon {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long id;
	@Column(nullable=false)
	protected String code;
	@Column(nullable=false)
	protected Double discountPercent;
	@Transient
	protected DateTimeRange duration;
	@Column(nullable=false)
	@Temporal(TemporalType.DATE)
	private Date startDate;
	@Column
	@Temporal(TemporalType.DATE)
	private Date endDate;
	@Transient
	protected Discountable target;
	@Column(nullable=false)
	private CouponType couponType;
	
	protected Coupon() {
		this.code = Hash.getStringHash(20);
	}
	
	protected Coupon(Double discountPercent, CouponType couponType) {
		this.code = Hash.getStringHash(20);
		this.discountPercent = discountPercent;
	}
	
	public static Coupon getAutomaticCoupon(Double discountPercent){
		return new Coupon(discountPercent, CouponType.AUTOMATIC_COUPON);
	}
	
	public static Coupon getNormalCoupon(Double discountPercent){
		return new Coupon(discountPercent, CouponType.NORMAL_COUPON);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public DateTimeRange getDuration() {
		return duration;
	}

	public void setDuration(DateTimeRange duration) {
		this.duration = duration;
	}

	public Discountable getTarget() {
		return target;
	}

	public void setTarget(Discountable target) {
		this.target = target;
	}
}
