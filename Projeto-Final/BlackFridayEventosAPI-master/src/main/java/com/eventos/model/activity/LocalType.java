package com.eventos.model.activity;

public enum LocalType {
	LABORATORY, ROOM, AUDITORY, HALL, SQUARE, BUILDING, ENTERPRISE;
}
