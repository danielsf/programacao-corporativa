package com.eventos.model.activity.event;

public enum EventStatus {
	NEW, OPEN_REGISTRATION, ONGOING, FINISHED, CANCELED;
}
