package com.eventos.model.activity.event;

public enum RelationshipType {
	PARTNERSHIP, SPONSORSHIP, COLABORATION, IDEALIZATION;
}
