package com.eventos.model.activity.registration;

public enum RegistrationStatus {
	STARTED, APROVED, DENIED;
}
