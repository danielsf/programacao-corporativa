package com.eventos.utils;

import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import net.sargue.mailgun.content.Body;

public final class EmailNotifier {
	private final String DOMAIN = "sandboxd0b16b8e00474a1aa88b2c1c5d38e426.mailgun.org";
	private final String KEY = "key-6dfcdfcdd1f344c7b10ea6dcb8076cb1";
	private final String FROM_NAME = "Daniel ADS";
	private final String FROM_EMAIL = "ddanielsilva661@gmail.com";
	private Configuration configuration;
	private Body body;
	
	public EmailNotifier() {
		configuration = new Configuration()
	    .domain(this.DOMAIN)
	    .apiKey(this.KEY)
	    .from(this.FROM_NAME, this.FROM_EMAIL);
		//Body:
		body = Body.builder()
        .h1("This is a trapezio descendente")
        .p("BIIIIRL")
        .build();
	}
	
	public void sendEmail(String email) {
		Mail.using(this.configuration)
	    .to(email)
	    .subject("HORA DO SHOW")
	    .content(this.body)
	    .build()
	    .send();
	}
	/**
	public static void main(String[] args) {
		EmailNotifier en = new EmailNotifier();
		en.sendEmail("b10h4z4rd01@gmail.com");
		en.sendEmail("ddanielsilva661@gmail.com");
		System.out.println("HAHAHA");
	}
	**/
}
