package com.eventos.utils;

import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;
import com.twilio.rest.api.v2010.account.Message;

public final class SMSNotifier {
	private final String ACCOUNT_SID = "ACe0469ed18654a813a6bc7986e7fcfc42";
	private final String AUTH_TOKEN = "a701161dd65ee16014a322ceeb1fc046";
	private final String FROM_NUMBER = "+18314065722";
	/**
	public static void main(String[] args) {
		SMSNotifier sn = new SMSNotifier();
		sn.sendMessage("+5586994428677");
		sn.sendMessage("+5586995210124");
		System.out.println("HEHE");
	}
	**/
	public void sendMessage(String number) {
		Twilio.init(this.ACCOUNT_SID, this.AUTH_TOKEN);

		Message message = Message.creator(
		    new PhoneNumber(number),  				// To number
		    new PhoneNumber(this.FROM_NUMBER),  	// From number
		    "Birl - Hora do Show 2.0!"              // SMS body
		).create();

		System.out.println(message.getSid());
	}
}
