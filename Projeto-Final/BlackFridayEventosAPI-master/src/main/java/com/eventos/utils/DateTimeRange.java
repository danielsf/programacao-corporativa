package com.eventos.utils;

import java.time.Instant;
import java.util.Date;

public class DateTimeRange implements Comparable<DateTimeRange>{
	private Date startDate;
	private Date endDate;
	
	public DateTimeRange() {
		this.startDate = Date.from(Instant.now());
		this.endDate = this.getFuture();
	}
	
	private Date getFuture() {
		return new Date(2100, 01, 01, 00, 00, 00);
	}

	public DateTimeRange(Date startDate, Date endDate) {		
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public boolean hasConflict(DateTimeRange dtr) {
		if(this.endDate.before(dtr.startDate) || this.endDate.equals(dtr.startDate)) {
			return false;
		}
		
		if(dtr.endDate.before(this.startDate) || dtr.endDate.equals(this.startDate)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		if(this.endDate.equals(Date.from(Instant.MAX)))
			return String.format("{\"%s\",\"UNKNOWN\"}", startDate);
		else
			return String.format("{\"%s\",\"%s\"}", startDate, endDate);
	}

	@Override
	public int compareTo(DateTimeRange dtr) {
		if(this.startDate.equals(dtr.startDate)) {
			return 0;  // Both Are Equals
		}else if(this.startDate.before(dtr.startDate)) { //IF a stars before b starts 
			return -1; //
		}else {
			return 1; //
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateTimeRange other = (DateTimeRange) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
}
