package com.eventos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenericValidator {
	
	public static void checkNull(Object object, String name) {
		if(object == null)
    		throw new RuntimeException(name + " cannot be null");	
	}
	
	public static void checkEmptyString(String item, String name) {
		checkNull(item, name);
		if(item.isEmpty())
    		throw new RuntimeException(name + " cannot be empty");	
	}
	
	public static boolean checkEmail(final String hex) {
		checkEmptyString(hex, "Email");
		Pattern pattern;
		Matcher matcher;
		
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}
}
