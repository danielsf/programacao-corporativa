package com.eventos.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import com.eventos.repository.ModelRepository;
@CrossOrigin
public interface AppController <T>{
	public ModelRepository<T, Long> getRepository();
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody default ResponseEntity<List<T>> index(){
		List<T> entities = getRepository().findAll();
		return new ResponseEntity<List<T>>(entities, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody default ResponseEntity<T> view(@PathVariable("id") Long id){
		T t = getRepository().findOne(id);
        if (t == null)
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<T>(t, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody default ResponseEntity<T> delete(Long id){
		T t = getRepository().findOne(id);
        if (t == null)
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
 
        getRepository().delete(id);
        return new ResponseEntity<T>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody default ResponseEntity<Void> add(@RequestBody T t, UriComponentsBuilder ucBuilder){
		getRepository().save(t);
        HttpHeaders headers = new HttpHeaders();
        //headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(t.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	@ResponseBody default ResponseEntity<T> edit(@PathVariable("id") Long id, @RequestBody T input){
		T t = getRepository().findOne(id);
        if (t == null)
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
        t = input;
        getRepository().save(t);
        return new ResponseEntity<T>(t, HttpStatus.OK);
	}
	
}
