package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.registration.Registration;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.RegistrationRepository;

@RestController
@RequestMapping("/registrations")
public class RegistrationController implements AppController<Registration>{
	
	@Autowired
	private RegistrationRepository registrationRepository;
	
	@Override
	public ModelRepository<Registration, Long> getRepository() {
		return this.registrationRepository;
	}
}
