package com.eventos.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.RegistrableActivity;
import com.eventos.repository.EventActivityRepository;
import com.eventos.repository.ModelRepository;

@RestController
@RequestMapping("/event_activities")
public class EventActivityController implements AppController<RegistrableActivity>{
	private EventActivityRepository  eventActivityRepository;
	@Override
	public ModelRepository<RegistrableActivity, Long> getRepository() {
		return this.eventActivityRepository;
	}
}
