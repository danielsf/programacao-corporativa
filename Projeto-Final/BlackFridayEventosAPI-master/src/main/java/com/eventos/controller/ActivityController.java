package com.eventos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.Activity;
import com.eventos.model.activity.event.Event;
import com.eventos.repository.ActivityRepository;
import com.eventos.repository.EventRepository;

@RestController
public class ActivityController{
	@Autowired
	private ActivityRepository activityRepository;
	@Autowired 
	private EventRepository eventRepository;
	
	@CrossOrigin
	@RequestMapping(path="/events/{id}/activities",method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Activity>> indexByUser(@PathVariable("id") Long id){
		Event event = this.eventRepository.findOne(id);
		if(event != null) {
			List<Activity> activities = event.getActivities();
			return new ResponseEntity<List<Activity>>(activities, HttpStatus.OK);
		}
		return new ResponseEntity<List<Activity>>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin
	@RequestMapping(path="/events/{idEvent}/activities/{id}",method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Activity> viewByUser(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id){
		Event event = this.eventRepository.findOne(idUser);
		if(event != null) {
			Activity activity = this.activityRepository.findOne(id);
			if(activity != null) {
				return new ResponseEntity<>(activity, HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}	
		
	@CrossOrigin
	@RequestMapping(path="/events/{id}/activities", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Void> add(@PathVariable("id") Long id, @RequestBody Activity activity){
		Event event = this.eventRepository.findOne(id);
		if(event != null) {
			event.addActivity(activity);;
			this.activityRepository.save(activity);
	        return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin	
	@RequestMapping(path = "/events/{idEvent}/activities/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Activity> delete(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id){
		Event event = this.eventRepository.findOne(id);
		Activity activity = this.activityRepository.findOne(idUser);
		
		if (event == null || activity == null)
            return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
 
        this.eventRepository.delete(id);
        return new ResponseEntity<Activity>(HttpStatus.NO_CONTENT);
	}
	
	@CrossOrigin
	@RequestMapping(path = "/events/{idEvent}/activities/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Activity> edit(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id, @RequestBody Activity input){
		Event event = this.eventRepository.findOne(idUser);
		if(event != null) {
			Activity activity = this.activityRepository.findOne(id);
	        if (activity == null)
	            return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
	        activity = input;
	        activity.setEvent(event);
	        this.activityRepository.save(activity);
	        return new ResponseEntity<Activity>(activity, HttpStatus.OK);
		}
		return new ResponseEntity<Activity>(HttpStatus.NOT_FOUND);
	}
}
