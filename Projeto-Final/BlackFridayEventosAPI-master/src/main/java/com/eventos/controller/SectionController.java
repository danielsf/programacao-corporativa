package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.Section;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.SectionRepository;

@RestController
@RequestMapping("/sections")
public class SectionController implements AppController<Section>{
	@Autowired
	private SectionRepository sectionRepository;
	
	@Override
	public ModelRepository<Section, Long> getRepository() {
		return this.sectionRepository;
	}
	
}
