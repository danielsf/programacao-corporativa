package com.eventos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.authentication.UserStatus;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.UserRepository;
import com.eventos.utils.Auth;
import com.eventos.utils.Hash;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController implements AppController<User>{
	@Autowired 
	private UserRepository userRepository;	
	
	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<User> login(@RequestBody Auth user) {
		List<User> users = userRepository.findByEmailAndPassword(user.getEmail(), Hash.getSHA1(user.getPassword()));
		if(users.size() == 0)
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		else
			return new ResponseEntity<User>(users.get(0), HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(value="{id}/confirm/{confirmationCode}", method = RequestMethod.GET)
	public ResponseEntity<User> confirm(@PathVariable("confirmationCode") String confirmationCode, @PathVariable("id") Long id) {
		User user = userRepository.findOne(id);
		if(user != null)
			if(user.isValidConfirmationCode(confirmationCode)) {
				user.setUserStatus(UserStatus.CONFIRMED);
				user.setConfirmationCode("");
				userRepository.save(user);
				return new ResponseEntity<User>(user, HttpStatus.OK);
			}
		return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
	}
	
	public ModelRepository<User, Long> getRepository() {
		return this.userRepository;
	}
}