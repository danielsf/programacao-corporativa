package com.eventos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.eventos.model.activity.authentication.User;
import com.eventos.model.activity.event.Event;
import com.eventos.repository.EventRepository;
import com.eventos.repository.UserRepository;


@RestController
public class EventController{
	@Autowired 
	private EventRepository eventRepository;
	@Autowired 
	private UserRepository userRepository;
	@CrossOrigin
	@RequestMapping(path="/events",method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Event>> index(){
		List<Event> entities = this.eventRepository.findAll();
		return new ResponseEntity<List<Event>>(entities, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(path="/users/{id}/events",method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Event>> indexByUser(@PathVariable("id") Long id){
		User user = this.userRepository.findOne(id);
		if(user != null) {
			List<Event> entities = user.getEvents();
			return new ResponseEntity<List<Event>>(entities, HttpStatus.OK);
		}
		return new ResponseEntity<List<Event>>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin
	@RequestMapping(path="/users/{idUser}/events/{id}",method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Event> viewByUser(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id){
		User user = this.userRepository.findOne(idUser);
		if(user != null) {
			Event event = this.eventRepository.findOne(id);
			if(event != null) {
				return new ResponseEntity<Event>(event, HttpStatus.OK);
			}
		}
		return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
	}	
	
	@CrossOrigin
	@RequestMapping(path = "/events/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Event> view(@PathVariable("id") Long id){
		Event event = this.eventRepository.findOne(id);
        if (event == null)
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<Event>(event, HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(path="/users/{id}/events", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Void> add(@PathVariable("id") Long id, @RequestBody Event event){
		User user = this.userRepository.findOne(id);
		if(user != null) {
			event.setCreator(user);
			this.eventRepository.save(event);
	        return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
	
	@CrossOrigin	
	@RequestMapping(path = "/users/{idUser}/events/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Event> delete(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id){
		User user = this.userRepository.findOne(idUser);
		Event event = this.eventRepository.findOne(id);
        if (event == null || user == null)
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
 
        this.eventRepository.delete(id);
        return new ResponseEntity<Event>(HttpStatus.NO_CONTENT);
	}
	
	@CrossOrigin
	@RequestMapping(path = "/users/{idUser}/events/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Event> edit(@PathVariable("idUser") Long idUser, @PathVariable("id") Long id, @RequestBody Event input){
		User user = this.userRepository.findOne(idUser);
		if(user != null) {
			Event event = this.eventRepository.findOne(id);
	        if (event == null)
	            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
	        event = input;
	        event.setCreator(user);
	        this.eventRepository.save(event);
	        return new ResponseEntity<Event>(event, HttpStatus.OK);
		}
		return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
	}
}