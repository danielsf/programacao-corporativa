package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.event.Partner;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.PartnerRepository;

@RestController
@RequestMapping("/partners")
public class PartnerController implements AppController<Partner>{
	@Autowired
	private PartnerRepository partnerRepository;
	
	@Override
	public ModelRepository<Partner, Long> getRepository() {
		return this.partnerRepository;
	}
}
