package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.Checkin;
import com.eventos.repository.CheckinRepository;
import com.eventos.repository.ModelRepository;

@RestController
@RequestMapping("/checkins")
public class CheckinController implements AppController<Checkin>{
	@Autowired
	private CheckinRepository checkinRepository;
	
	@Override
	public ModelRepository<Checkin, Long> getRepository() {
		return this.checkinRepository;
	}
}
