package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.Responsable;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.ResponsableRepository;

@RestController
@RequestMapping("/responsables")
public class ResponsableController implements AppController<Responsable>{
	@Autowired
	private ResponsableRepository responsableRepository;

	@Override
	public ModelRepository<Responsable, Long> getRepository() {
		return this.responsableRepository;
	}
	
}
