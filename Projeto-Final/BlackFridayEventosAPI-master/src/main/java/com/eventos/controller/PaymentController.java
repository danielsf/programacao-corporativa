package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.payment.Payment;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.PaymentRepository;


@RestController
@RequestMapping("/payments")
public class PaymentController implements AppController<Payment>{
	@Autowired
	private PaymentRepository paymentRepository;

	@Override
	public ModelRepository<Payment, Long> getRepository() {
		return this.paymentRepository;
	}
}
