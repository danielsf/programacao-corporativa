package com.eventos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventos.model.activity.Tag;
import com.eventos.repository.ModelRepository;
import com.eventos.repository.TagRepository;

@RestController
@RequestMapping("/tags")
public class TagController implements AppController<Tag>{
	
	@Autowired 
	private TagRepository tagRepository;
	
	@Override
	public ModelRepository<Tag, Long> getRepository() {
		return this.tagRepository;
	}
	
}
