# Friday Team

## BlackFridayEventosAPI

Restful API of Events Application

### Prototype (V1):
	
	Web platform which can handle event data in order to improve the user experience with web platform for events.

## Project Artifacts

### Entity Relationship Diagram

![Entity Relationship Diagram](https://drive.google.com/uc?export=view&id=0B75J-X4YCDCDNk1sM1JCd0g4Tmc)

### Class Diagram

![Class Diagram](https://drive.google.com/uc?export=view&id=0B75J-X4YCDCDNVJYdHBNa2JKWTg)

### Use Case Diagram

![Use Case Diagram](https://drive.google.com/uc?export=view&id=0B75J-X4YCDCDSW1oRVc5SDF3NVE)

### Component Diagram

![Component Diagram](https://drive.google.com/uc?export=view&id=0B75J-X4YCDCDek5DRWRwemhPTm8)


## Members:

* [Alexsander Magnum](https://github.com/10Alexsander10)
* [Daniel Farias](https://github.com/danieldsf)
* [Mateus Oliveira](https://github.com/M4T3U5)
* [Wildrimak Pereira](https://github.com/Wildrimak)

### Technologies:

* [Spring Framework](https://spring.io/)
* [JPA/Hibernate](http://docs.oracle.com/javaee/6/tutorial/doc/bnbpz.html)
* [REST Architecture](http://www.restapitutorial.com/lessons/whatisrest.html)
* [Flask](http://flask.pocoo.org/)
* [NPM](https://www.npmjs.com/)
* [Jquery](https://jquery.com/)
* [Angular](https://angularjs.org/)
* [Bootstrap (Yeti)](https://bootswatch.com/yeti/)
* [Android](https://www.android.com/)

### Another cool stuffs

[Project Planning](https://trello.com/b/2w91uXMv)

[Deploy URL](https://blackfridayeventosapi.herokuapp.com/)

	
