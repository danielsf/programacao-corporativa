package utils;

/**
 * Created by Alexsander on 12/08/2017.
 */

public class ServidorUtils {


    final static String baseURL = "http://192.168.0.8:8080/";

    final static String userRoute = "user/";

    final static String userLoginRoute = "user/login/";

    public String getBaseURL(){
        return baseURL;
    }

    public static String getUserRoute(){
        return baseURL + userRoute;
    }

    public static String getUserLoginRoute(){
        return baseURL + userLoginRoute;
    }
}
