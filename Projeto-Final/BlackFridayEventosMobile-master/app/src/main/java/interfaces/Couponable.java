package interfaces;

public interface Couponable {
	Double getDiscountPercent();
}
