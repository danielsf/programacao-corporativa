package interfaces;

public interface Discountable {
	
	Double getValue();
	
}
