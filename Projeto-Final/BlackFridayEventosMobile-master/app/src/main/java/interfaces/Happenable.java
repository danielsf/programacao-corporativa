package interfaces;

import model.Event;

public interface Happenable {
	Event getEvent();
}
