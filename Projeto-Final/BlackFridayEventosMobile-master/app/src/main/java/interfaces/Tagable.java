package interfaces;
import java.util.*;
import model.Tag;

public interface Tagable {
	
	List<Tag> getTags();
}
