package fridayapp.br.com.fridayeventsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.StrictMode;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import controller.UserController;
import model.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.ConexaoUtils;

public class MainActivity extends AppCompatActivity {

    EditText editname;
    EditText editpass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editname = (EditText)findViewById(R.id.userEditLogin);
        editpass = (EditText)findViewById(R.id.userEditPassword);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


    public void doLogin(View view) throws IOException {

        setContentView(R.layout.auth_loading);

        String email = editname.getText().toString();
        String senha = editpass.getText().toString();


        if (email.equals("")) {
            Toast.makeText(this,R.string.empty_user_email, Toast.LENGTH_SHORT).show();
        }

        if (senha.equals("")) {
            Toast.makeText(this, R.string.empty_user_pass, Toast.LENGTH_SHORT).show();
        }
        if (email.equals("") || senha.equals("")) {
            Toast.makeText(this, R.string.user_and_password_empty, Toast.LENGTH_SHORT).show();
            finish();
        } else {

            if (!email.equals("") || !senha.equals("")) {

                User user = new User(email,senha);

                Intent intent = new Intent(this, ValidateAutentication.class);
                intent.putExtra("userEmail", email);
                intent.putExtra("userPass", senha);
                startActivity(intent);

//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(ConexaoUtils.API_BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//
//                UserController service = retrofit.create(UserController.class);
//
//                Call<Object> authObjectCall = service.loginResponse(user);
//
//
//                if (authObjectCall.execute().code() == ConexaoUtils.HTTPRequestCodes.SUCCESS) {
//                    Toast.makeText(this, "SUCESSO", Toast.LENGTH_SHORT).show();
//                }else{
//                    Toast.makeText(this, "FALHA", Toast.LENGTH_SHORT).show();
//                    setContentView(R.layout.activity_main);
//                }
            }
        }
    }

    public void toCreateAccountActivity(View view){

        Intent intent = new Intent(this,CreateAccountActivity.class);
        startActivity(intent);
    }


}
