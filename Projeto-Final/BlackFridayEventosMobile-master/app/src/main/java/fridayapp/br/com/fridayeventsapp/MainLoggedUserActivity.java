package fridayapp.br.com.fridayeventsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainLoggedUserActivity extends AppCompatActivity {

    TextView userWelcome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_logged_user);

        Bundle extras = getIntent().getExtras();

        userWelcome = (TextView)findViewById(R.id.loggedMainUserName);
        String nome = (extras.getString("user","vazio"));

        userWelcome.setText("bem vindo, ");

        Toast.makeText(this, nome, Toast.LENGTH_LONG).show();
    }

    public void toMyProfile(View view){

        Intent intent = new Intent(this,UserViewProfileActivity.class);
        startActivity(intent);

    }
}
