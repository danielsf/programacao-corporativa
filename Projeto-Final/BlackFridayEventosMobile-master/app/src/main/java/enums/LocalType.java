package enums;

public enum LocalType {
	LABORATORY, ROOM, AUDITORY, HALL, SQUARE, BUILDING, ENTERPRISE;
}
