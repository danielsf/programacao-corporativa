package model;

import java.util.ArrayList;
import java.util.List;

import enums.UserStatus;
import utils.Hash;

/**
 * Created by Home on 20/08/2017.
 */

public class User {


    private Long id;
    private String fullname;
    private String email;
    private String password;
    private String confirmationCode;
    private UserStatus userStatus;

    private List<Event> events;

    private List<Tag> tags;

    private List<Registration> registrations;

    private List<Administration> administrations;

    private User() {
        this.events = new ArrayList<>();
        this.registrations = new ArrayList<>();
        this.tags = new ArrayList<>();
        this.administrations = new ArrayList<>();
        this.userStatus = UserStatus.UNCONFIRMED;
        this.confirmationCode = Hash.getStringHash(20);
    }

    public User(String email, String password) {
        this();
        this.email = email;
        this.password = password;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public List<Administration> getAdministrations() {
        return administrations;
    }

    public void setAdministrations(List<Administration> administrations) {
        this.administrations = administrations;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getFullname() {
        return fullname;
    }

    //Login:
    public boolean checkLogin(String email, String password) {
        return this.email.equals(email) && this.password.equals(password);
    }

    public boolean isValidConfirmationCode(String confirmationCode) {
        return this.confirmationCode.equals(confirmationCode);
    }

    static User getEmptyUser() {
        return new User();
    }

    public String toString() {
        return String.format(
                "{id=%s, fullname=%s, email=%s, password=%s, confirmationCode=%s, tags=%s}",
                id, fullname, email, password, confirmationCode, tags);
    }

}
