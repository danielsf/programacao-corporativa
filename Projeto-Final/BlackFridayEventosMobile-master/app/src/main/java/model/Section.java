package model;

import java.util.ArrayList;
import java.util.List;

public class Section {
	private Long id;
	private String name;
	private List<Tag> tags;
	private List<EventActivity> eventActivities;
	private List<User> coordinators;
	private List<String> coupons;
	
	private Section() {
		this.tags = new ArrayList<>();
		this.eventActivities = new ArrayList<>();
		this.coordinators = new ArrayList<>();
		this.coupons = new ArrayList<>();
	}
	
	public Section(String name) {
		this();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<EventActivity> getEventActivities() {
		return eventActivities;
	}

	public void setEventActivities(List<EventActivity> eventActivities) {
		this.eventActivities = eventActivities;
	}

	public List<User> getCoordinators() {
		return coordinators;
	}

	public void setCoordinators(List<User> coordinators) {
		this.coordinators = coordinators;
	}

	public List<String> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<String> coupons) {
		this.coupons = coupons;
	}
}
