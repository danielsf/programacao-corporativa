package model;


public class Local{
	private Integer id;
	private String name;
	private Address address;
	
	public Local(){
		
	}
	
	public Integer getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public Address getAddress(){
		return this.address;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setAddress(Address address){
		this.address = address;
	}
}
