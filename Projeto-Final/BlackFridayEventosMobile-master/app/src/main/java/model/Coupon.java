package model;


import interfaces.Discountable;
import utils.*;


public abstract class Coupon {
	protected Long id;
	protected String code;
	protected Double discountPercent;
	protected DateTimeRange duration;
	protected Discountable target;
	
	protected Coupon() {
		this.code = Hash.getStringHash(20);
	}
	
	protected static DiscountCoupon getDiscountCoupon(Double discountPercent){
		return DiscountCoupon.getDiscountCoupon(discountPercent);
	}
	
	protected static TemporaryCoupon getTemporaryCoupon(Double discountPercent){
		return TemporaryCoupon.getTemporaryCoupon(discountPercent);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public DateTimeRange getDuration() {
		return duration;
	}

	public void setDuration(DateTimeRange duration) {
		this.duration = duration;
	}

	public Discountable getTarget() {
		return target;
	}

	public void setTarget(Discountable target) {
		this.target = target;
	}
}
