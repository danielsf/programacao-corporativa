package model;

public class DiscountCoupon extends Coupon{
	private DiscountCoupon() {
		super();
	}
	
	private DiscountCoupon(Double discountPercent) {
		this();
		this.discountPercent = discountPercent;
	}
	
	public static DiscountCoupon getDiscountCoupon(Double discountPercent) {
		return new DiscountCoupon(discountPercent);
	}
}
