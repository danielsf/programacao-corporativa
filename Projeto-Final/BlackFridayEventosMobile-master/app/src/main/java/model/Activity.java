package model;

import java.util.ArrayList;

import enums.ActivityType;
import interfaces.Happenable;
import utils.DateTimeRange;

/**
 * Created by Home on 20/08/2017.
 */

public class Activity implements Happenable{

    private Integer id;
    private String name;
    private String description;
    private ActivityType activityType;
    private DateTimeRange duration;
    private Event event;
    private ArrayList<Responsable> responsables;

    private Activity(){
        this.activityType = ActivityType.MINICOURSE;
        this.responsables = new ArrayList<>();
    }

    public Activity(String name) {
        this();
        this.name = name;

    }

    public boolean isAGrandLecture(){
        return this.activityType.equals(ActivityType.GRANDE_LECTURE);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public ArrayList<Responsable> getResponsables() {
        return responsables;
    }

    public void setResponsables(ArrayList<Responsable> responsables) {
        this.responsables = responsables;
    }
}

