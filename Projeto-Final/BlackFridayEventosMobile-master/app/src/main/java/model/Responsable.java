package model;



import enums.ResponsableType;


public class Responsable {
	private Long id;
	private String name;
	private String description;
	private String profile;
	private ResponsableType responsableType;


	public Responsable() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public ResponsableType getResponsableType() {
		return responsableType;
	}

	public void setResponsableType(ResponsableType responsableType) {
		this.responsableType = responsableType;
	}
}
