package controller;

import java.util.List;

import model.Event;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Home on 20/08/2017.
 */

public interface EventController {

    @GET("events/")
    Call<List<Event>> getAllEvents();

    @POST("users/{userID}/events")
    Call<List<Object>> createEvent (@Body Event newEvent);

    @DELETE("users/{id}/events/{eventId}")
    Call<List<Object>> deleteEventById (@Path("id") Integer userId, @Path("eventId") Integer eventId);

    @GET("/users/{id}/events/{eventId}")
    Call<List<Event>> findUserEventById (@Path("id") Integer userId, @Path("eventId") Integer eventId);

    @POST("/users/{id}/events/{eventId}")
    Call<List<Event>> updateEventById (@Path("id") Integer userId, @Path("eventId") Integer eventId );
}
