package com.eventos.utils;
import com.eventos.enums.*;

public class Message {
	private String message;
	private MessageType type;
	
	private Message() {
		this.message = "";
		this.type = MessageType.SUCCESS;
	}
	
	private Message(String message, MessageType type) {
		this.message = message;
		this.type = type;
	}
	
	public static Message createErrorMessage(String message) {
		return new Message(message, MessageType.ERROR);
	}
	
	public static Message createSuccessMessage(String message) {
		return new Message(message, MessageType.SUCCESS);
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public MessageType getType() {
		return this.type;
	}
	
	public void setType(MessageType type) {
		this.type = type;
	}
}
