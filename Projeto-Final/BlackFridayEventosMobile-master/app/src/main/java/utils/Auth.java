package com.eventos.utils;

public class Auth {
	private String email;
	private String password;
	
	public Auth() {
		this.email = "";
		this.password = "";
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getPassword() {
		return this.password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
