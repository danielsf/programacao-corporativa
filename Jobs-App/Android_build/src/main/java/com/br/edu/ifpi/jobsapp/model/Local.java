package br.edu.ifpi.jobsapp.model;

public class Local {

	private String nome;
	private int hashcode;

	public Local() {
		this.hashcode = hashCode();
	}
	
	public Local(String lista[] ){
		this.nome = lista[0];
		this.hashcode = Integer.parseInt(lista[1]);
	}

	public Local(String nome){

		this.nome = nome;
		this.hashcode = hashCode();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Local other = (Local) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Local [nome=" + nome + "]";
	}
	
	// PLEASE DO NOT MODIFI THIS METHOD
	
	public String toSave(){
		return this.nome+","+this.hashcode;
	}	
}


