package com.br.edu.ifpi.jobsapp.utils;
import br.edu.ifpi.jobsapp.model.*;
import android.content.*;
import android.*;
import android.app.*;
import java.util.*;

public class DataPersist extends Activity
{
	
	private static String dbLocals = "locals";
	private static String dbEmpresas = "empresas";
	private static final String KEY = "objeto";
	static SharedPreferences sp;
	private static List<Local> locais;
	private int tamanho;
	
	public void saveLocal(Local local){
		sp = getSharedPreferences(dbLocals,0);
		SharedPreferences.Editor edit = sp.edit();
		tamanho = sp.getInt("tamanho",0);
		edit.putString(KEY+tamanho,local.toSave());
	}
	
	
	public List<Local> getLocals(){
		
		sp = getSharedPreferences(dbLocals,0);
		tamanho = sp.getInt("tamanho",0);
		for (int i = 0; i < tamanho; i++){		
			locais.add( new Local(sp.getString(KEY+i,"").split(",")));		
		}
		
		return locais;
	}
	
	
}
