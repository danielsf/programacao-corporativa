package br.edu.feicibuque.interfaces;

import br.edu.feicibuque.model.Message;

public interface Comunicable {
	
	public void sendMessage(Message message);
}
