package br.edu.feicibuque.interfaces;

import java.util.ArrayList;

import br.edu.feicibuque.model.Content;

public interface Publishable {
	
	public void post(ArrayList<Content> content);	
}
