package br.edu.feicibuque.interfaces;

public interface Localizable {
	public double getLatitude();
	public double getLongitude();
	
	public default double[] getLatitudeLongitude(){
		double location[] = new double[2];
		location[0] = this.getLatitude();
		location[1] = this.getLongitude();
		return location;
	}
}
