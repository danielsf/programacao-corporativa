package br.edu.feicibuque.enums;

public enum RelationshipType {
	FRIEND("Friendship"), FOLLOWER("Follower"), BLOCKED("Blocked"), REQUESTED("Requested");
	
	private String value;  
	
	private RelationshipType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
