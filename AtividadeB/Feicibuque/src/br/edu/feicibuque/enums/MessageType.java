package br.edu.feicibuque.enums;

public enum MessageType {
	READ("Read"), SENT("Sent"), RECEIVED("Received");
	
	private String value;  
	
	private MessageType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
