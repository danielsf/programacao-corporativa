package br.edu.feicibuque.enums;

public enum PermissionType {
	COMMON("Common"), ADMINISTRATOR("Administrator");
	
	private String value;  
	
	private PermissionType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
