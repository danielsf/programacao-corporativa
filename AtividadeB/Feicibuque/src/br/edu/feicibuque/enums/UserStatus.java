package br.edu.feicibuque.enums;

public enum UserStatus {
	ONLINE("Online"), OFFLINE("Offline"), INACTIVE("Inactive");
	
	private String value;  
	
	private UserStatus(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
