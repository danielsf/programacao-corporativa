package br.edu.feicibuque.enums;

public enum PostType {
	POST("Post"), SHARE("Share"), 
	PROFILE_PICTURE("Profile Picture"), COVER_PHOTO("Cover Photo"),
	CHECK_IN("Check-in");
	
	private String value;  
	
	private PostType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
