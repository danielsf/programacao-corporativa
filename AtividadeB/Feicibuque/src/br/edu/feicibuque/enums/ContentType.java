package br.edu.feicibuque.enums;

public enum ContentType {
	AUDIO("Audio"), IMAGE("Image"), TEXT("Text"), URL("Url"), VIDEO("Video");
	
	private String value;  
	
	private ContentType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
