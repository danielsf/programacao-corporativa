package br.edu.feicibuque.enums;

public enum GroupType {
	OPEN("Open"),CLOSED("Closed"),SECRET("Secret");
	
	private String value;  
	
	private GroupType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
