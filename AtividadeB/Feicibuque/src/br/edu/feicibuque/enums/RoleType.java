package br.edu.feicibuque.enums;

public enum RoleType {
	ADMINISTRATOR("Administrator"), MEMBER("Member");
	
	private String value;  
	
	private RoleType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
