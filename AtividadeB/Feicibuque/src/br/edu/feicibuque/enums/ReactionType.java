package br.edu.feicibuque.enums;

public enum ReactionType {
	LIKE("Like"), THANKFUL("Thankful"),ANGRY("Angry"), SAD("Sad"),SMILE("Smile");
	
	private String value;  
	
	private ReactionType(String value){  
		this.setValue(value);  
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
