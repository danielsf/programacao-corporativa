package br.edu.feicibuque.model;

import br.edu.feicibuque.interfaces.Localizable;

public class Point implements Localizable{
	private String label;
	private Place place;
	private double longitude;
	private double latitude;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "Point [label=" + label + ", place=" + place + ", longitude=" + longitude + ", latitude=" + latitude
				+ "]";
	}
	
	
	
}
