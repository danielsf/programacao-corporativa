package br.edu.feicibuque.model;

import java.util.Date;

import br.edu.feicibuque.enums.MessageType;
import br.edu.feicibuque.interfaces.Comunicable;

public class Message {
	private MessageType messageType;
	private String body;
	
	private Comunicable sender;
	private Comunicable receiver;
	
	private Date sendDate;
	private Date readDate;
	//
	public MessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Comunicable getSender() {
		return sender;
	}
	public void setSender(Comunicable sender) {
		this.sender = sender;
	}
	public Comunicable getReceiver() {
		return receiver;
	}
	public void setReceiver(Comunicable receiver) {
		this.receiver = receiver;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public Date getReadDate() {
		return readDate;
	}
	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
		result = prime * result + ((readDate == null) ? 0 : readDate.hashCode());
		result = prime * result + ((receiver == null) ? 0 : receiver.hashCode());
		result = prime * result + ((sendDate == null) ? 0 : sendDate.hashCode());
		result = prime * result + ((sender == null) ? 0 : sender.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (messageType != other.messageType)
			return false;
		if (readDate == null) {
			if (other.readDate != null)
				return false;
		} else if (!readDate.equals(other.readDate))
			return false;
		if (receiver == null) {
			if (other.receiver != null)
				return false;
		} else if (!receiver.equals(other.receiver))
			return false;
		if (sendDate == null) {
			if (other.sendDate != null)
				return false;
		} else if (!sendDate.equals(other.sendDate))
			return false;
		if (sender == null) {
			if (other.sender != null)
				return false;
		} else if (!sender.equals(other.sender))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Message [messageType=" + messageType + ", body=" + body + ", sender=" + sender + ", receiver="
				+ receiver + ", sendDate=" + sendDate + ", readDate=" + readDate + "]";
	}
	
	
	
}
