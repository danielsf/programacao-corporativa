package br.edu.feicibuque.model;

public class Share {
	private Post post;
	private Post referencia;
	
	public Post getPost() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}
	
	public Post getReferencia() {
		return referencia;
	}
	
	public void setReferencia(Post referencia) {
		this.referencia = referencia;
	}
	
	@Override
	public String toString() {
		return "Share [post=" + post + ", referencia=" + referencia + "]";
	}
}
