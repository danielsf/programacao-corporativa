package br.edu.feicibuque.model;

import java.util.ArrayList;
import java.util.Date;

import br.edu.feicibuque.interfaces.Manageable;

public class Group implements Manageable{
	
	private ArrayList<Role> members;
	private String label;
	private User owner;
	private Date createdDate;
	
	public ArrayList<Role> getMembers() {
		return members;
	}
	public void setMembers(ArrayList<Role> members) {
		this.members = members;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "Group [members=" + members + ", label=" + label + ", owner=" + owner + ", createdDate=" + createdDate
				+ "]";
	}
	
	
	
	

}
