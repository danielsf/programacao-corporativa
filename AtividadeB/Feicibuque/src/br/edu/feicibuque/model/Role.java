package br.edu.feicibuque.model;

import java.util.Date;

import br.edu.feicibuque.enums.RoleType;

public class Role {
	private User user;
	private RoleType roleType;
	private Group group;
	private Date createdDate;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public RoleType getRoleType() {
		return roleType;
	}
	public void setRoleType(RoleType roleType) {
		this.roleType = roleType;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "Role [user=" + user + ", roleType=" + roleType + ", group=" + group + ", createdDate=" + createdDate
				+ "]";
	} 
	
	
	
	
}
