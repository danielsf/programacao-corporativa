package br.edu.feicibuque.model;

import java.util.ArrayList;
import java.util.Date;

import br.edu.feicibuque.interfaces.Comunicable;
import br.edu.feicibuque.interfaces.Publishable;

public class Comment implements Publishable{
	private Comunicable owner;
	
	private Publishable source;
	private Date createdDate;
	
	private ArrayList<Content> contents;
	private ArrayList<Comment> replies;
	private ArrayList<Reaction> reactions;
	private ArrayList<User> taggedUsers;
	
	@Override
	public void post(ArrayList<Content> content) {
		this.contents = content;
	}

	public Comunicable getOwner() {
		return owner;
	}

	public void setOwner(Comunicable owner) {
		this.owner = owner;
	}

	public ArrayList<Content> getContents() {
		return contents;
	}

	public void setContents(ArrayList<Content> contents) {
		this.contents = contents;
	}

	

	public Publishable getSource() {
		return source;
	}

	public void setSource(Publishable source) {
		this.source = source;
	}

	public ArrayList<Comment> getReplies() {
		return replies;
	}

	public void setReplies(ArrayList<Comment> replies) {
		this.replies = replies;
	}

	@Override
	public String toString() {
		return "Comment [owner=" + owner + ", source=" + source + ", createdDate="
				+ createdDate + ", contents=" + contents + ", replies=" + replies + ", reactions=" + reactions
				+ ", taggedUsers=" + taggedUsers + "]";
	}

	

	public ArrayList<Reaction> getReactions() {
		return reactions;
	}

	public void setReactions(ArrayList<Reaction> reactions) {
		this.reactions = reactions;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
}
