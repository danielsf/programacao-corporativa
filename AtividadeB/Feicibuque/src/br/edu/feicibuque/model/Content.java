package br.edu.feicibuque.model;

import br.edu.feicibuque.enums.ContentType;

public class Content {
	private String body;
	private ContentType contentType;
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((body == null) ? 0 : body.hashCode());
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Content other = (Content) obj;
		if (body == null) {
			if (other.body != null)
				return false;
		} else if (!body.equals(other.body))
			return false;
		if (contentType != other.contentType)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Content [body=" + body + ", contentType=" + contentType + "]";
	}
	
	
	
}
