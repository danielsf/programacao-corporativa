package br.edu.feicibuque.model;

import java.util.Date;
import br.edu.feicibuque.enums.RelationshipType;

public class Relationship {
	private User sender;
	private User receiver;
	private RelationshipType relationshipType;
	private Date createdDate;
	
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	public User getReceiver() {
		return receiver;
	}
	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	public RelationshipType getRelationshipType() {
		return relationshipType;
	}
	public void setRelationshipType(RelationshipType relationshipType) {
		this.relationshipType = relationshipType;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "Relationship [sender=" + sender + ", receiver=" + receiver + ", relationshipType=" + relationshipType
				+ ", createdDate=" + createdDate + "]";
	}
	
	

}
