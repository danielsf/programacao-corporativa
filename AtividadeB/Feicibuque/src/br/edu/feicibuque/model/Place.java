package br.edu.feicibuque.model;

import br.edu.feicibuque.interfaces.Localizable;

public class Place implements Localizable{
	private String city;
	private String area;
	private String country;
	private double latitude;
	private double longitude;
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Place [city=" + city + ", area=" + area + ", country=" + country + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}
}
