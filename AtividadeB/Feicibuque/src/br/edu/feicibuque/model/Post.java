package br.edu.feicibuque.model;

import java.util.ArrayList;
import java.util.Date;


import br.edu.feicibuque.enums.PostType;
import br.edu.feicibuque.interfaces.Comunicable;
import br.edu.feicibuque.interfaces.Localizable;
import br.edu.feicibuque.interfaces.Publishable;

public class Post implements Publishable{
	private Comunicable owner;
	private Date createdDate;
	private PostType postType;

	private ArrayList<Content> contents;
	private ArrayList<Comment> comments;	
	private ArrayList<Date> modifiedDate; 
	private ArrayList<Reaction> reactions;
	private ArrayList<User> taggedUsers;
	private Localizable local;
	
	@Override
	public void post(ArrayList<Content> content) {
		this.contents = content;
	}

	public Comunicable getOwner() {
		return owner;
	}

	public void setOwner(Comunicable owner) {
		this.owner = owner;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public PostType getPostType() {
		return postType;
	}

	public void setPostType(PostType postType) {
		this.postType = postType;
	}

	public ArrayList<Content> getContents() {
		return contents;
	}

	public void setContents(ArrayList<Content> contents) {
		this.contents = contents;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public ArrayList<Date> getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(ArrayList<Date> modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public ArrayList<Reaction> getReactions() {
		return reactions;
	}

	public void setReactions(ArrayList<Reaction> reactions) {
		this.reactions = reactions;
	}

	@Override
	public String toString() {
		return "Post [owner=" + owner + ", createdDate=" + createdDate + ", postType=" + postType + ", contents="
				+ contents + ", comments=" + comments + ", modifiedDate=" + modifiedDate + ", reactions=" + reactions
				+ ", taggedUsers=" + taggedUsers + ", local=" + local + "]";
	}

	public Localizable getLocal() {
		return local;
	}

	public void setLocal(Localizable local) {
		this.local = local;
	}

	public ArrayList<User> getTaggedUsers() {
		return taggedUsers;
	}

	public void setTaggedUsers(ArrayList<User> taggedUsers) {
		this.taggedUsers = taggedUsers;
	}
	
	
}
