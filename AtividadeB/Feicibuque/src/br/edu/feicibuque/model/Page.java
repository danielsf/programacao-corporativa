package br.edu.feicibuque.model;

import java.util.ArrayList;

import br.edu.feicibuque.interfaces.Comunicable;
import br.edu.feicibuque.interfaces.Manageable;

public class Page implements Comunicable, Manageable{
	private User owner;
	private ArrayList<Post> timeline;
	
	private ArrayList<Role> administrators;
	private ArrayList<Message> messages;
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public ArrayList<Post> getTimeline() {
		return timeline;
	}

	public void setTimeline(ArrayList<Post> timeline) {
		this.timeline = timeline;
	}

	
	
	public void sendMessage(Message message){
		this.messages.add(message);
	}

	public ArrayList<Role> getAdministrators() {
		return administrators;
	}

	public void setAdministrators(ArrayList<Role> administrators) {
		this.administrators = administrators;
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((administrators == null) ? 0 : administrators.hashCode());
		result = prime * result + ((messages == null) ? 0 : messages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Page other = (Page) obj;
		if (administrators == null) {
			if (other.administrators != null)
				return false;
		} else if (!administrators.equals(other.administrators))
			return false;
		if (messages == null) {
			if (other.messages != null)
				return false;
		} else if (!messages.equals(other.messages))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Page [owner=" + owner + ", timeline=" + timeline + ", administrators=" + administrators + ", messages="
				+ messages + "]";
	}
	
	
}
