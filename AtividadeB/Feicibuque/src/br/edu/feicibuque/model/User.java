package br.edu.feicibuque.model;

import java.util.ArrayList;

import br.edu.feicibuque.enums.UserStatus;
import br.edu.feicibuque.interfaces.Comunicable;
import br.edu.feicibuque.interfaces.Publishable;

public class User implements Comunicable{
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String phone;
	private boolean verified;
	
	private UserStatus userStatus;
	private Place birthPlace;
	private Place currentPlace;
	
	private ArrayList<Message> messages;
	private ArrayList<Publishable> timeline;
	
	private ArrayList<Relationship> userRelationships;
	private ArrayList<Relationship> relationshipsOnUser;
	
	private ArrayList<Role> roles;
	
	public ArrayList<Relationship> getUserRelationships() {
		return userRelationships;
	}

	public void setUserRelationships(ArrayList<Relationship> userRelationships) {
		this.userRelationships = userRelationships;
	}

	public ArrayList<Relationship> getRelationshipsOnUser() {
		return relationshipsOnUser;
	}

	public ArrayList<Role> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<Role> roles) {
		this.roles = roles;
	}

	public void setRelationshipsOnUser(ArrayList<Relationship> relationshipsOnUser) {
		this.relationshipsOnUser = relationshipsOnUser;
	}

	
	
	public void sendMessage(Message message){
		this.messages.add(message);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Place getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(Place birthPlace) {
		this.birthPlace = birthPlace;
	}

	public Place getCurrentPlace() {
		return currentPlace;
	}

	public void setCurrentPlace(Place currentPlace) {
		this.currentPlace = currentPlace;
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}

	public ArrayList<Publishable> getTimeline() {
		return timeline;
	}

	public void setTimeline(ArrayList<Publishable> timeline) {
		this.timeline = timeline;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password=" + password
				+ ", phone=" + phone + ", verified=" + verified + ", userStatus=" + userStatus + ", birthPlace="
				+ birthPlace + ", currentPlace=" + currentPlace + ", messages=" + messages + ", timeline=" + timeline
				+ ", userRelationships=" + userRelationships + ", relationshipsOnUser=" + relationshipsOnUser
				+ ", roles=" + roles + "]";
	}
	
	
	
	
	
}
