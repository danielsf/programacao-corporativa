package br.edu.feicibuque.model;

public class Reply {
	private Comment comment;
	private Comment parent;
	
	public Comment getComment() {
		return comment;
	}
	
	public void setComment(Comment comment) {
		this.comment = comment;
	}
	
	public Comment getParent() {
		return parent;
	}
	
	public void setParent(Comment parent) {
		this.parent = parent;
	}
	
	@Override
	public String toString() {
		return "Reply [comment=" + comment + ", parent=" + parent + "]";
	}
}
