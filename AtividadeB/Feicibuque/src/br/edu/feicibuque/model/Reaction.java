package br.edu.feicibuque.model;

import java.util.Date;

import br.edu.feicibuque.enums.ReactionType;
import br.edu.feicibuque.interfaces.Publishable;

public class Reaction {
	private User owner;
	private Publishable source;
	private ReactionType reactionType;
	private Date createDate;
	
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Publishable getSource() {
		return source;
	}
	public void setSource(Publishable source) {
		this.source = source;
	}
	public ReactionType getReactionType() {
		return reactionType;
	}
	public void setReactionType(ReactionType reactionType) {
		this.reactionType = reactionType;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public String toString() {
		return "Reaction [owner=" + owner + ", source=" + source + ", reactionType=" + reactionType + ", createDate="
				+ createDate + "]";
	}
	
	
	
}
